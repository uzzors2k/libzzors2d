# README #

Version 1.0.0

### What is this repository for? ###

libzzors2d is a basic opengl library which (hopefully) abstracts away most of the mathematics knowledge, and shader/fragment programs into simple to use objects for some standard graphics types.
Classes for drawing textures, sprites, text, and arbitrary geometric shapes in any color are provided. Shouldn't require much boilerplate code.
Focus has only been on 2D graphics so far, hence the name.

### How do I get set up? ###

Clone the repository, and open it using Android studio. The test app should demonstrate basic usage.
Unit tests should also help illustrate the usage. Sorry I haven't prepared better documentation here.
The library itself can be included in any android app as a module.

### Contribution guidelines ###

Not currently planning on actively developing this, but if you have ideas let me know!

### Who do I talk to? ###

uzzors2k