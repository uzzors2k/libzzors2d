package com.uzzors2k.opengltest.DemoClasses;

import android.content.Context;

import com.uzzors2k.libzzors2d.particles.ParticleGenerator;
import com.uzzors2k.libzzors2d.particles.ParticleGeneratorPool;
import com.uzzors2k.libzzors2d.particles.ParticleShaderProgram;
import com.uzzors2k.libzzors2d.particles.SimpleParticleGenerator;
import com.uzzors2k.libzzors2d.shapes.Point;
import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Demonstrates usage of particles classes
 */

public class ParticlesDemo {
    private ParticleShaderProgram particleShaderProgram;
    
    private final ParticleGenerator testParticleShooter;
    private final ParticleGenerator lineParticleShooter;
    private final SimpleParticleGenerator manualParticleShooter;
    
    private final ParticleGeneratorPool particlePool;
    
    private final Random random = new Random();
    
    private boolean alreadyRemovedManualParticleGen = false;
    
    public ParticlesDemo() {
        particlePool = new ParticleGeneratorPool();
        
        // Move the following code to OnSurfaceCreated?
        PointColor greenColor = new PointColor(0.0f, 1.0f, 0.0f, 0.5f);
        PointColor blueColor = new PointColor(0.0f, 0.0f, 1.0f, 0.5f);
        PointColor redColor = new PointColor(1.0f, 0.0f, 0.0f, 0.5f);
        
        // Point source particle generator
        Point spawnPositionA = new Point(new Coordinate(-0.6f, 1.2f), redColor);
        Coordinate startingDirection = new Coordinate(0.5f, -0.4f);
        Coordinate gravityDirection = new Coordinate(-0.4f, 0.2f);
        float speedVariance = 0.3f;
        float angleVariance = 0.7f;
        float decayFactor = 1.0f;
        
        final int maxParticles = 1024;
        testParticleShooter = new ParticleGenerator(maxParticles, spawnPositionA, spawnPositionA,
                startingDirection, gravityDirection, speedVariance, angleVariance, decayFactor);
        
        // Line particle generator
        Point lineSpawnA = new Point(new Coordinate(-1.0f, -1.2f), greenColor);
        Point lineSpawnB = new Point(new Coordinate(1.0f, -1.2f), blueColor);
        Coordinate lineStartDir = new Coordinate(0, 0.5f);
        Coordinate lineGravity = new Coordinate(0, -0.6f);
        float lineSpeedVary = 0.1f;
        float lineAngleVary = 0.2f;
        
        lineParticleShooter = new ParticleGenerator(maxParticles, lineSpawnA, lineSpawnB,
                lineStartDir, lineGravity, lineSpeedVary, lineAngleVary, decayFactor);
        
        manualParticleShooter = new SimpleParticleGenerator(maxParticles);
        
        particlePool.addDrawable(testParticleShooter);
        particlePool.addDrawable(lineParticleShooter);
        particlePool.addDrawable(manualParticleShooter);
    }
    
    public void onSurfaceCreated(Context context) {
        particleShaderProgram = new ParticleShaderProgram(context);
    }
    
    public void updateInternalObjects(float currentTime, boolean timerFinished) {
        testParticleShooter.addParticles(1, currentTime);
        lineParticleShooter.addParticles(5, currentTime);
        
        if (timerFinished) {
            timerFinished();
        } else {
            calculateNewParticleLocations(currentTime);
        }
    }
    
    public void draw(float[] projectionMatrix, float currentTime) {
        particlePool.updateTotalFloatArray();
        
        particleShaderProgram.useProgram();
        particleShaderProgram.setUniforms(projectionMatrix, currentTime);
        particlePool.drawWithShader(particleShaderProgram);
    }
    
    private void timerFinished() {
        if (!alreadyRemovedManualParticleGen) {
            alreadyRemovedManualParticleGen = true;
            particlePool.removeObject(manualParticleShooter);
        }
    }
    
    private void calculateNewParticleLocations(float currentTime) {
        float decayFactor = 0.35f;
        PointColor randomColor = new PointColor(random.nextFloat(), random.nextFloat(), random
                .nextFloat(), 1.0f);
        
        float R1 = 0.25f;
        float theta = 0.2f * currentTime * (float) Math.PI;
        float originCircleX = R1 * (float) cos(theta);
        float originCircleY = R1 * (float) sin(theta);
        
        Coordinate direction = new Coordinate(-originCircleX * random.nextFloat(), -originCircleY
                * random.nextFloat());
        
        float gravityFactor = 0.3f;
        Coordinate gravity = new Coordinate(-originCircleX, -originCircleY);
        gravity.multiply(gravityFactor);
        
        float circleRadius = 0.45f;
        float R2 = circleRadius * (float) cos(theta) - circleRadius * (float) sin(theta);
        float startX = R2 * (float) cos(theta);
        float startY = R2 * (float) sin(theta);
        Coordinate startingPos = new Coordinate(startX, startY);
        
        manualParticleShooter.addParticle(startingPos, randomColor, direction, gravity,
                decayFactor, currentTime);
    }
}
