package com.uzzors2k.opengltest.DemoClasses;

import android.content.Context;

import com.uzzors2k.libzzors2d.shapes.ColorShaderProgram;
import com.uzzors2k.libzzors2d.shapes.Line;
import com.uzzors2k.libzzors2d.shapes.Point;
import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.shapes.Polygon;
import com.uzzors2k.libzzors2d.shapes.ShapesPool;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

/**
 * Demonstration of simple drawable objects
 */

public class SimpleDrawablesDemo {
    private ColorShaderProgram colorShaderProgram;
    
    private final ShapesPool simpleObjectsPool;
    
    private final Polygon box1;
    private final Polygon box2;
    private final Polygon box3;
    
    // Lines
    private final Point point1;
    private final Point point2;
    private final Point point3;
    
    private final Coordinate point1Direction;
    private final Coordinate point2Direction;
    private final Coordinate point3Direction;

    // Last touch location
    private final Coordinate touchPoint;
    
    private float lastUpdateTime;
    
    private boolean alreadyRemovedBoxes = false;
    
    public SimpleDrawablesDemo() {
        simpleObjectsPool = new ShapesPool();
    
        // Initialize the objects being drawn
        PointColor greenColor = new PointColor(0.0f, 1.0f, 0.0f, 0.5f);
        PointColor blueColor = new PointColor(0.0f, 0.0f, 1.0f, 0.5f);
        PointColor redColor = new PointColor(1.0f, 0.0f, 0.0f, 0.5f);
        PointColor whiteColor = new PointColor(1.0f, 1.0f, 1.0f, 0.7f);
        PointColor touchColor = new PointColor(0.1f, 0.1f, 0.0f, 1.0f);

        // Create single point
        Point centerCoord = new Point(new Coordinate(0.0f, 0.0f), redColor);
        simpleObjectsPool.addDrawable(centerCoord);

        touchPoint = new Coordinate(0.1f, 0.1f);
        Point touchCoord = new Point(touchPoint, touchColor);
        simpleObjectsPool.addDrawable(touchCoord);

        // Create line objects
        point1Direction = new Coordinate(0.1f, 0.1f);
        point2Direction = new Coordinate(-0.1f, 0.5f);
        point3Direction = new Coordinate(-0.2f, 0.3f);
    
        point1 = new Point(new Coordinate(-0.3f, 0.7f), greenColor);
        point2 = new Point(new Coordinate(0.9f, -0.6f), redColor);
        point3 = new Point(new Coordinate(-0.9f, 0.6f), blueColor);
    
        Line testLine1 = new Line(point1, point2, 20);
        Line testLine2 = new Line(point2, point3, 10);
        Line testLine3 = new Line(point3, point1, 15);
    
        testLine1.zLayer = -1;
        testLine2.zLayer = -2;
        testLine3.zLayer = -3;
    
        lastUpdateTime = 0;
    
        simpleObjectsPool.addDrawable(testLine1);
        simpleObjectsPool.addDrawable(testLine2);
        simpleObjectsPool.addDrawable(testLine3);
    
        // Polygon objects
        Point box1Point0 = new Point(new Coordinate(0.5f, 0.5f), greenColor);
        Point box1Point1 = new Point(new Coordinate(0.7f, 0.5f), greenColor);
        Point box1Point2 = new Point(new Coordinate(0.7f, 0.7f), whiteColor);
        Point box1Point3 = new Point(new Coordinate(0.5f, 0.7f), greenColor);
    
        Point box2Point0 = new Point(new Coordinate(0.5f, 0.5f), blueColor);
        Point box2Point1 = new Point(new Coordinate(0.7f, 0.5f), whiteColor);
        Point box2Point2 = new Point(new Coordinate(0.7f, 0.7f), blueColor);
        Point box2Point3 = new Point(new Coordinate(0.5f, 0.7f), blueColor);
    
        Point box3Point0 = new Point(new Coordinate(0.5f, 0.5f), whiteColor);
        Point box3Point1 = new Point(new Coordinate(0.7f, 0.5f), redColor);
        Point box3Point2 = new Point(new Coordinate(0.7f, 0.7f), redColor);
        Point box3Point3 = new Point(new Coordinate(0.5f, 0.7f), redColor);
    
        box1 = new Polygon();
        box1.addPolygonPoint(box1Point0);
        box1.addPolygonPoint(box1Point1);
        box1.addPolygonPoint(box1Point2);
        box1.addPolygonPoint(box1Point3);
    
        box2 = new Polygon();
        box2.addPolygonPoint(box2Point0);
        box2.addPolygonPoint(box2Point1);
        box2.addPolygonPoint(box2Point2);
        box2.addPolygonPoint(box2Point3);
    
        box3 = new Polygon();
        box3.addPolygonPoint(box3Point0);
        box3.addPolygonPoint(box3Point1);
        box3.addPolygonPoint(box3Point2);
        box3.addPolygonPoint(box3Point3);
    
        simpleObjectsPool.addDrawable(box1);
        simpleObjectsPool.addDrawable(box2);
        simpleObjectsPool.addDrawable(box3);
    
        // Demonstrating alterations after adding objects
        box1.zLayer = 2;
        box2.zLayer = 1;
        box3.zLayer = 0;
    
        box1.offsetEntirePolygon(new Coordinate(-0.05f, -0.05f));
        box2.offsetEntirePolygon(new Coordinate(0.05f, 0.05f));
    
        simpleObjectsPool.updateTotalFloatArray();
    }
    
    public void onSurfaceCreated(Context context) {
        colorShaderProgram = new ColorShaderProgram(context);
    }
    
    public void draw(float[] projectionMatrix) {
        simpleObjectsPool.updateTotalFloatArray();
        
        colorShaderProgram.useProgram();
        colorShaderProgram.setUniforms(projectionMatrix);
        simpleObjectsPool.drawWithShader(colorShaderProgram);
    }
    
    public void updateInternalObjects(float currentTime, boolean timerFinished,
                                      Coordinate lastTouchLocation, boolean relayerBoxes) {

        // DOn't change the reference, just the values
        touchPoint.x = lastTouchLocation.x;
        touchPoint.y = lastTouchLocation.y;

        if (relayerBoxes) {
            box1.zLayer = wrapAroundCount(box1.zLayer, 2);
            box2.zLayer = wrapAroundCount(box2.zLayer, 2);
            box3.zLayer = wrapAroundCount(box3.zLayer, 2);
        }
        
        if (timerFinished && !alreadyRemovedBoxes) {
            alreadyRemovedBoxes = true;
            
            simpleObjectsPool.removeObject(box1);
            simpleObjectsPool.removeObject(box2);
            simpleObjectsPool.removeObject(box3);
            simpleObjectsPool.updateTotalFloatArray();
        }
    
        updateLinePositions(currentTime);
    }
    
    private void updateLinePositions(float currentTime) {
        float elapsedTime = currentTime - lastUpdateTime;
        lastUpdateTime = currentTime;
        
        // Calc new positions
        Coordinate position1Change = new Coordinate(point1Direction);
        position1Change.multiply(elapsedTime);
        Coordinate position2Change = new Coordinate(point2Direction);
        position2Change.multiply(elapsedTime);
        Coordinate position3Change = new Coordinate(point3Direction);
        position3Change.multiply(elapsedTime);
        
        point1.location.add(position1Change);
        point2.location.add(position2Change);
        point3.location.add(position3Change);
        
        // Constrain positions to within a box
        final float minX = -1.0f;
        final float maxX = 1.0f;
        final float minY = -1.0f;
        final float maxY = 1.0f;
        
        if (!withinBounds(point1.location.x, minX, maxX)) {
            point1Direction.x *= -1.0f;
        }
        if (!withinBounds(point1.location.y, minY, maxY)) {
            point1Direction.y *= -1.0f;
        }
        
        if (!withinBounds(point2.location.x, minX, maxX)) {
            point2Direction.x *= -1.0f;
        }
        if (!withinBounds(point2.location.y, minY, maxY)) {
            point2Direction.y *= -1.0f;
        }
        
        if (!withinBounds(point3.location.x, minX, maxX)) {
            point3Direction.x *= -1.0f;
        }
        if (!withinBounds(point3.location.y, minY, maxY)) {
            point3Direction.y *= -1.0f;
        }
    }
    
    private boolean withinBounds(float num, float minBound, float maxBound) {
        return (num > minBound) && (num < maxBound);
    }
    
    private int wrapAroundCount(int currentCount, int maxCount) {
        int nowCount = currentCount + 1;
        if (nowCount > maxCount) nowCount = 0;
        return nowCount;
    }
}
