package com.uzzors2k.opengltest.DemoClasses;

import android.content.Context;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.sprites.Sprite;
import com.uzzors2k.libzzors2d.sprites.SpritePool;
import com.uzzors2k.libzzors2d.sprites.TextureShaderProgram;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.utils.resources.TextureHelper;
import com.uzzors2k.opengltest.R;

/**
 * Demonstration of sprites
 */

public class SpriteDemo {
    private TextureShaderProgram textureShaderProgram;
    
    private final SpritePool braidSpritePool;
    private Sprite braidSprite1;
    private final Sprite braidSprite2;

    private final SpritePool tinySpritePool;
    private final Sprite tinySprite;
    
    private int braidTexture;
    private int tinyTexture;

    private boolean alreadyRemovedSprite = false;
    
    private final int BRAID_COLUMNS = 7;
    private final int BRAID_ROWS = 4;

    // Based on the image size
    private static final int BRAID_TEXTURE_WIDTH = 520;
    private static final int BRAID_TEXTURE_HEIGHT = 347;

    // Simply the normalized width/height divided by the pixel count in each direction
    private final Coordinate BRAID_HALF_PIXEL_SIZE = TextureHelper.calculateHalfPixelSize(
            BRAID_TEXTURE_WIDTH, BRAID_TEXTURE_HEIGHT);

    private int currentBraidFrame = 0;
    private int tinyFramePrescaler = 0;

    public SpriteDemo() {
        braidSpritePool = new SpritePool(BRAID_COLUMNS, BRAID_ROWS);

        int TINY_ROWS = 2;
        int TINY_COLUMNS = 2;

        int TINY_CELL_SIZE_X = 23;
        int TINY_CELL_SIZE_Y = 23;
        Coordinate TINY_HALF_PIXEL_SIZE = TextureHelper.calculateHalfPixelSize(TINY_COLUMNS, TINY_ROWS,
                TINY_CELL_SIZE_X, TINY_CELL_SIZE_Y);

        tinySpritePool = new SpritePool(TINY_COLUMNS, TINY_ROWS);

        braidSprite1 = new Sprite(0, 0, 1, 1,
                BRAID_COLUMNS, BRAID_ROWS, BRAID_HALF_PIXEL_SIZE);
        braidSprite1.setCurrentFrame(0);
        braidSprite1.setSpriteSize(new Coordinate(1.0f, 1.0f));
        braidSprite1.setPosition(new Coordinate(-0.2f, 0.6f), RotatableBox.Alignment.BOTTOM_LEFT);
        braidSprite1.setSpriteMirroring(true, true);
        braidSprite1.setSpriteRotationInRadians(Math.PI / 8);
        braidSpritePool.addDrawable(braidSprite1);
    
        braidSprite2 = new Sprite(0, 0, 1, 1,
                BRAID_COLUMNS, BRAID_ROWS, BRAID_HALF_PIXEL_SIZE);
        braidSprite2.setCurrentFrame(0);
        braidSprite2.setSpriteSize(new Coordinate(0.5f, 0.5f));
        braidSprite2.setPosition(new Coordinate(-0.2f, -0.6f), RotatableBox.Alignment.TOP_RIGHT);
        braidSprite2.setSpriteMirroring(false, false);

        PointColor red = new PointColor(1.0f, 0.0f, 0.0f, 1.0f);
        PointColor green = new PointColor(0.0f, 1.0f, 0.0f, 1.0f);
        PointColor blue = new PointColor(0.0f, 0.0f, 1.0f, 1.0f);
        PointColor white = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        braidSprite2.setColorFilter(red, green, blue, white);

        braidSpritePool.addDrawable(braidSprite2);

        tinySprite = new Sprite(0, 0, 1, 1,
                TINY_COLUMNS, TINY_ROWS, TINY_HALF_PIXEL_SIZE);
        tinySprite.setCurrentFrame(0);
        tinySprite.setSpriteSize(new Coordinate(0.4f, 0.4f));
        tinySprite.setPosition(new Coordinate(-0.55f, 0.1f), RotatableBox.Alignment.TOP_RIGHT);

        PointColor yellow = new PointColor(1.0f, 1.0f, 0.0f, 1.0f);
        tinySprite.setRedReplacement(yellow);
        tinySprite.setBlueReplacement(white);

        tinySpritePool.addDrawable(tinySprite);
    }
    
    public void onSurfaceCreated(Context context) {
        textureShaderProgram = new TextureShaderProgram(context);
        braidTexture = TextureHelper.loadTexture(context, R.drawable.braid);
        tinyTexture = TextureHelper.loadTexture(context, R.drawable.tiny_sprite);
    }
    
    public void updateInternalObjects(boolean timerFinished, boolean stepAnimations) {
        if (stepAnimations) {
            // Skip the final blank frame
            currentBraidFrame++;
            int MAX_FRAME = (BRAID_COLUMNS * BRAID_ROWS) - 2;
            if (currentBraidFrame > MAX_FRAME) currentBraidFrame = 0;
    
            braidSprite1.setCurrentFrame(currentBraidFrame);
            braidSprite2.setCurrentFrame(currentBraidFrame);

            // Only step the tiny sprite at a fraction of the braid sprite
            tinyFramePrescaler++;
            if (tinyFramePrescaler > 15) {
                tinyFramePrescaler = 0;
                tinySprite.stepCurrentFrame();
            }
        }
        if (timerFinished && !alreadyRemovedSprite) {
            alreadyRemovedSprite = true;
            braidSpritePool.removeObject(braidSprite1);

            braidSprite1 = new Sprite(0, 0, 1, 1,
                    BRAID_COLUMNS, BRAID_ROWS, BRAID_HALF_PIXEL_SIZE);
            braidSprite1.setCurrentFrame(0);
            braidSprite1.setSpriteSize(new Coordinate(0.2f, 0.2f));
            braidSprite1.setPosition(new Coordinate(-0.5f, -0.4f), RotatableBox.Alignment.BOTTOM_LEFT);
            braidSprite1.setSpriteMirroring(true, true);
            braidSprite1.setSpriteRotationInRadians(Math.PI / 3);
            braidSpritePool.addDrawable(braidSprite1);

            tinySprite.resetColoringMode();
        }
    }
    
    public void draw(float[] projectionMatrix) {
        braidSpritePool.updateTotalFloatArray();
        tinySpritePool.updateTotalFloatArray();

        textureShaderProgram.useProgram();

        textureShaderProgram.setUniforms(projectionMatrix, braidTexture);
        braidSpritePool.drawWithShader(textureShaderProgram);

        textureShaderProgram.setUniforms(projectionMatrix, tinyTexture);
        tinySpritePool.drawWithShader(textureShaderProgram);
    }
}
