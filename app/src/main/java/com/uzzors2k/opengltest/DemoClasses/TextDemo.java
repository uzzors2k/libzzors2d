package com.uzzors2k.opengltest.DemoClasses;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.sprites.TextureShaderProgram;
import com.uzzors2k.libzzors2d.text.TextBox;
import com.uzzors2k.libzzors2d.text.TextTextureCreator;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;

import java.util.Locale;

/**
 * Demonstration of text boxes
 */

public class TextDemo {
    
    private TextureShaderProgram textureShaderProgram;
    
    private TextBox frameRateText;
    private TextBox testText;
    private TextBox countDownText;
    
    private int textTexture;
    
    private boolean alreadyRemovedText = false;
    
    public void onSurfaceCreated(Context context, AssetManager assets) {
        textureShaderProgram = new TextureShaderProgram(context);
        
        // Create a bitmap sprite sheet for text. Could also use a custom defined sprite sheet.
        TextTextureCreator textureCreator = new TextTextureCreator(assets,
                "Roboto-Regular.ttf", 64, 4, 4);
        textTexture = textureCreator.getTextTextureId();

        PointColor darkGreen = new PointColor(0, 0.6f, 0.2f, 1.0f);

        testText = new TextBox(textureCreator.getAtlasProperties());
        testText.setCharacterSize(new Coordinate(0.1f, 0.1f));
        testText.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.BOTTOM_CENTER);
        testText.setText("012345 ABCDEF abcdef {|}~");
        testText.setTextBoxRotationRadians(-Math.PI / 4);
        testText.setColorFilter(darkGreen);
        
        frameRateText = new TextBox(textureCreator.getAtlasProperties());
        frameRateText.setText("FPS:");
        frameRateText.setCharacterSize(new Coordinate(0.1f, 0.1f));
        frameRateText.setPosition(new Coordinate(0.95f, 1.75f), RotatableBox.Alignment.TOP_RIGHT);
        frameRateText.setColorFilter(PointColor.createFromAndroidColor(Color.MAGENTA));

        PointColor darkRed = new PointColor(0.9f, 0.3f, 0.3f, 1.0f);
        PointColor darkRedLowAlpha = new PointColor(0.9f, 0.3f, 0.3f, 0.3f);

        countDownText = new TextBox(textureCreator.getAtlasProperties());
        countDownText.setText("10.00");
        countDownText.setCharacterSize(new Coordinate(0.2f, 0.2f));
        countDownText.setPosition(new Coordinate(0.0f, -0.3f), RotatableBox.Alignment.CENTER);
        countDownText.setColorFilter(darkRed, darkRed, darkRedLowAlpha, darkRedLowAlpha);
    }
    
    public void setFrameRateText(long actualFrameRate) {
        String fpsString = String.format(Locale.ENGLISH, "FPS: %d", actualFrameRate);
        frameRateText.setText(fpsString);
    }
    
    public void draw(float[] projectionMatrix) {
        textureShaderProgram.useProgram();
        textureShaderProgram.setUniforms(projectionMatrix, textTexture);

        testText.drawWithShader(textureShaderProgram);
        frameRateText.drawWithShader(textureShaderProgram);
        countDownText.drawWithShader(textureShaderProgram);
    }
    
    public void updateInternalObjects(boolean timerFinished, float remainingTime) {
        if (timerFinished && !alreadyRemovedText) {
            alreadyRemovedText = true;
            countDownText.setText("00:00");
            countDownText.visible = false;
        } else if (!alreadyRemovedText) {
            int remainingSeconds = (int) remainingTime;
            int remainingMilliseconds = (int) ((remainingTime % 1.0f) * 100);
            String countDownString = String.format(Locale.ENGLISH,"%02d:%02d", remainingSeconds,
                    remainingMilliseconds);
            countDownText.setText(countDownString);
        }
    }
}
