package com.uzzors2k.opengltest;

import android.content.Context;
import android.content.res.AssetManager;
import android.opengl.GLSurfaceView.Renderer;
import android.os.SystemClock;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.FrameRateLimiter;
import com.uzzors2k.libzzors2d.utils.IntervalTimer;
import com.uzzors2k.libzzors2d.utils.ScreenProjection;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.opengltest.DemoClasses.ParticlesDemo;
import com.uzzors2k.opengltest.DemoClasses.SimpleDrawablesDemo;
import com.uzzors2k.opengltest.DemoClasses.SpriteDemo;
import com.uzzors2k.opengltest.DemoClasses.TextDemo;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_BLEND;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_ONE;
import static android.opengl.GLES20.GL_ONE_MINUS_SRC_ALPHA;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;

class GLRenderer implements Renderer {
    private final Context context;
    private final AssetManager assets;
    
    private final ParticlesDemo particlesDemo;
    private final SimpleDrawablesDemo simpleDrawablesDemo;
    private final SpriteDemo spriteDemo;
    private final TextDemo textDemo;
    
    private final ScreenProjection screenProjection;
    private final FrameRateLimiter frameRateLimiter;
    
    private long globalStartTime;
    
    // Sprite time counter
    private final IntervalTimer spriteTimer;
    private final IntervalTimer boxesTimer;
    private float remainingTime = 10.0f;
    private float lastFrameDrawnTime = 0;
    private boolean timerFinished = false;
    private boolean timerPaused = false;
    private Coordinate lastTouchLocation;
    
    GLRenderer(Context context) {
        this.context = context;
        assets = context.getAssets();
        
        particlesDemo = new ParticlesDemo();
        simpleDrawablesDemo = new SimpleDrawablesDemo();
        spriteDemo = new SpriteDemo();
        textDemo = new TextDemo();
    
        screenProjection = new ScreenProjection();
        frameRateLimiter = new FrameRateLimiter();
    
        final int SPRITE_FRAME_RATE = 30;
        final int BOX_RELAYER_TIME = 2;
        spriteTimer = new IntervalTimer(SPRITE_FRAME_RATE);
        boxesTimer = new IntervalTimer(BOX_RELAYER_TIME);

        lastTouchLocation = new Coordinate(0, 0);
    }
    
    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        // Set some openGl options
        PointColor whiteColor = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClearColor(whiteColor.getRedComponent(), whiteColor.getGreenComponent(), whiteColor.getBlueComponent(), whiteColor
                .getAlphaComponent());
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

        particlesDemo.onSurfaceCreated(context);
        simpleDrawablesDemo.onSurfaceCreated(context);
        spriteDemo.onSurfaceCreated(context);
        textDemo.onSurfaceCreated(context, assets);
        
        // Set up time for particles system, and others
        globalStartTime = System.nanoTime();
        timerFinished = false;
    }
    
    /**
     * onSurfaceChanged is called whenever the surface has changed. This is
     * called at least once when the surface is initialized. Keep in mind that
     * Android normally restarts an Activity on rotation, and in that case, the
     * renderer will be destroyed and a new one created.
     *
     * @param width  The new width, in pixels.
     * @param height The new height, in pixels.
     */
    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);
        screenProjection.onSurfaceChanged(width, height);
    }
    
    /**
     * OnDrawFrame is called whenever a new frame needs to be drawn. Normally,
     * this is done at the refresh rate of the screen.
     */
    @Override
    public void onDrawFrame(GL10 glUnused) {
        // NOTE: 	Objects are layered according to the order in which drawWithShader in
        // 			called on these drawablePools. For drawing e.g. sprite_A under all particles,
        //			and sprite_B on top of all particles, create two spritePools and draw them
        //			before and after the particle pool.
        
        long actualFrameRate = frameRateLimiter.limitFrameRate(60);
        textDemo.setFrameRateText(actualFrameRate);
    
        float currentTime = (System.nanoTime() - globalStartTime) / 1000000000f;
        
        // Count down until removing some objects from the screen
        if (!timerPaused) {
            remainingTime -= (currentTime - lastFrameDrawnTime);
            if (!timerFinished && (remainingTime <= 0)) {
                timerFinished = true;
            }
        }
        lastFrameDrawnTime = currentTime;
    
        // Calculate new object states
        textDemo.updateInternalObjects(timerFinished, remainingTime);
        spriteDemo.updateInternalObjects(timerFinished, spriteTimer.updateIntervalTimer(SystemClock.elapsedRealtime()));
        simpleDrawablesDemo.updateInternalObjects(currentTime, timerFinished, lastTouchLocation,
                boxesTimer.updateIntervalTimer(SystemClock.elapsedRealtime()));
        particlesDemo.updateInternalObjects(currentTime, timerFinished);
        
        glClear(GL_COLOR_BUFFER_BIT);
        
        textDemo.draw(screenProjection.getProjectionMatrix());
        spriteDemo.draw(screenProjection.getProjectionMatrix());
        simpleDrawablesDemo.draw(screenProjection.getProjectionMatrix());
        particlesDemo.draw(screenProjection.getProjectionMatrix(), currentTime);
    }
    
    void handleTouchPress(float touchX, float touchY) {
        lastTouchLocation = screenProjection.getTouchCoordinatesOnScreen(touchX, touchY);

        // Check if press is near the center of the screen, if so pause the count-down
        Coordinate touchLocation = new Coordinate(lastTouchLocation);
        Coordinate touchCenter = new Coordinate(0.0f, 0.0f);
        final float acceptedRadius = 0.2f;
        
        if (touchLocation.subtract(touchCenter).getMagnitude() < acceptedRadius) {
            timerPaused = !timerPaused;
        }
    }
}
