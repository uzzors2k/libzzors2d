package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.VertexArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by uzzors2k on 10.07.2017
 * Base class for all drawable pools. Handles correctly
 * setting up vertex data, and attaching the correct shader.
 */
public abstract class DrawablePool<T1 extends SimpleDrawable, T2 extends ShaderProgram> {
    protected static final int POSITION_COMPONENT_COUNT = 2;

    protected VertexArray vertexArray;
    protected final List<T1> drawableObjectsPool;

    private float[] totalObjectPoolFloatArray;
    private int poolBufferOffset;
    
    public DrawablePool() {
        totalObjectPoolFloatArray = new float[0];
        drawableObjectsPool = new ArrayList<>();
        poolBufferOffset = 0;
    }
    
    /**
     * Attach a shader to the drawable pool. This shader will be used to draw all items in the pool
     *
     * @param program shader program to draw with
     */
    protected abstract void attachShader(T2 program);
    
    /**
     * @return a float array containing all data which will be passed from CPU to GPU for all
     * items in the drawable pool
     */
    public float[] getFloatBufferData() {
        return totalObjectPoolFloatArray;
    }
    
    /**
     * @param newDrawableItem item which should be drawn when this pool receives a draw request
     */
    public void addDrawable(T1 newDrawableItem) {
        drawableObjectsPool.add(newDrawableItem);
    }

    /**
     * @param newDrawableItems item which should be drawn when this pool receives a draw request
     */
    public void addAllDrawable(List<T1> newDrawableItems) {
        drawableObjectsPool.addAll(newDrawableItems);
    }
    
    /**
     * Run through all drawable items, and update the buffer index for each, and also recreate
     * the float buffer which is passed to the GPU. Should be called anytime a change is made to
     * a drawable item in the list
     */
    public void updateTotalFloatArray() {
        // Sort here in case the Z ordering has been changed
        Collections.sort(drawableObjectsPool);
        
        totalObjectPoolFloatArray = new float[0];
        int currentIndex = 0;
        for (T1 drawableItem : drawableObjectsPool) {
            drawableItem.setBufferIndex(poolBufferOffset + currentIndex);
            float[] drawableBufferContents = drawableItem.getFloatBufferData();
            currentIndex += drawableItem.getVertexSize();
            
            totalObjectPoolFloatArray = VertexArray.concatFloatArrays(totalObjectPoolFloatArray,
                    drawableBufferContents);
        }
        
        // Create a raw data stream to send to the OpenGL engine
        vertexArray = new VertexArray(totalObjectPoolFloatArray);
    }
    
    /**
     * Draw items in the drawable pool to the screen, using the specified shader program
     *
     * @param program shader program to draw all items in the drawable pool with
     */
    public void drawWithShader(T2 program) {
        boolean firstVisibleDrawableFound = false;

        for (T1 drawableItem : drawableObjectsPool) {
            if (drawableItem.visible) {
                // Attach the shader only if there are items to draw
                if (!firstVisibleDrawableFound) {
                    firstVisibleDrawableFound = true;
                    attachShader(program);
                }
                performPreDrawEvents(program, drawableItem);
                drawableItem.draw();
            }
        }
    }

    /**
     * Perform any required events before drawing the current item. Typically setting uniforms in
     * the shader program.
     * @param program Reference to the rendering program
     * @param drawable Reference to the drawable item currently being rendered.
     */
    protected abstract void performPreDrawEvents(T2 program, T1 drawable);
    
    /**
     * Remove all drawables from the drawable pool
     */
    public void clearDrawablesPool() {
        drawableObjectsPool.clear();
    }
    
    /**
     * Remove the item at the given index from the list. Bounds checking is performed.
     *
     * @param index index of the drawable to remove
     * @return true if the index was valid and an object was removed, false otherwise
     */
    public boolean removeObject(int index) {
        if ((index >= 0) && (index < drawableObjectsPool.size())) {
            drawableObjectsPool.remove(index);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Remove the object with the given reference from the drawable pool
     *
     * @param object reference of the object to remove from the list
     * @return true if the reference was valid and an object was removed, false otherwise
     */
    public boolean removeObject(T1 object) {
        return drawableObjectsPool.remove(object);
    }
    
    /**
     * @param offset offset added to all drawable items in the drawable pool, when locating start
     *               point of a specific item in the total float array passed to the GPU
     */
    public void setPoolBufferOffset(int offset) {
        poolBufferOffset = offset;
        updateTotalFloatArray();
    }

    public boolean removeAllObjects(List<T1> objectsList) {
        boolean objectsRemoved = false;
        for (T1 object: objectsList) {
            objectsRemoved |= removeObject(object);
        }
        return objectsRemoved;
    }
}
