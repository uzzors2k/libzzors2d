package com.uzzors2k.libzzors2d;

import android.content.Context;

import com.uzzors2k.libzzors2d.utils.ShaderHelper;
import com.uzzors2k.libzzors2d.utils.resources.TextResourceReader;

import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUseProgram;

/***
 * Excerpted from "OpenGL ES for Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/kbogla for more book information.
 ***/
abstract public class ShaderProgram {
    // Uniform constants
    private static final String U_MATRIX = "u_Matrix";
    
    // Attribute constants
    protected static final String A_POSITION = "a_Position";
    
    // Shader program
    protected final int program;
    
    // Uniform locations
    protected final int uMatrixLocation;
    
    /**
     * @param context                  resources context
     * @param vertexShaderResourceId   resource ID of the text file containing the vertex shader
     *                                 program
     * @param fragmentShaderResourceId resource ID of the text file containing the fragment
     *                                 shader program
     */
    protected ShaderProgram(Context context, int vertexShaderResourceId,
                            int fragmentShaderResourceId) {
        // Compile the shaders and link the program.
        program = ShaderHelper.buildProgram(
                TextResourceReader.readTextFileFromResource(
                        context, vertexShaderResourceId),
                TextResourceReader.readTextFileFromResource(
                        context, fragmentShaderResourceId));
        
        // Retrieve uniform locations for the shader program.
        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);
    }
    
    /**
     * Set the current OpenGL shader program to this program.
     */
    public void useProgram() {
        glUseProgram(program);
    }
    
    abstract public int getPositionAttributeLocation();
}
