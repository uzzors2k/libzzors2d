package com.uzzors2k.libzzors2d;

/**
 * Interface for basic OpenGL drawable items
 */
public abstract class SimpleDrawable implements Comparable<SimpleDrawable> {
    
    /**
     * Sets the order to draw in, in the current drawables pool. Higher number places on top,
     * lower on bottom.
     */
    public int zLayer;

    /**
     * Sets whether or not to actually draw the item.
     */
    public boolean visible = true;
    
    /**
     * @return the number of data points which will be passed to the shaders
     */
    abstract public int getVertexSize();
    
    /**
     * @return a float array containing all data which will be passed from CPU to GPU for this
     * specific item
     */
    abstract public float[] getFloatBufferData();
    
    /**
     * @param index starting point in the total drawable pool float buffer for this specific
     *              drawable
     */
    abstract public void setBufferIndex(int index);
    
    /**
     * Draws the item on the screen, using a shader program defined by the drawable pool
     */
    abstract protected void draw();
    
    /**
     * Override of the equals method, used when removing items from the drawable pool
     *
     * @param obj other object to compare against
     * @return true if both objects are considered equal, which in this case means all important
     * fields
     */
    abstract public boolean equals(Object obj);
    
    @Override
    public int compareTo(SimpleDrawable another) {
        if (this.zLayer < another.zLayer) {
            return -1;
        } else {
            return 1;
        }
    }
}
