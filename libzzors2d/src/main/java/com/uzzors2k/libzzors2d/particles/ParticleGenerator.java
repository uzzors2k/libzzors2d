package com.uzzors2k.libzzors2d.particles;

import com.uzzors2k.libzzors2d.shapes.Point;
import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import java.util.Random;

/**
 * Particles are created with an initial state, and the GPU calculates where and what
 * they are at the current time. Once enough time elapses the particles will just alpha
 * away into nothing, but still exist. Hence the upper limit MAX_PARTICLE_COUNT
 * In other words, communication is one way. Particles are created with an initial state
 * here, and realized in the GPU
 */

public class ParticleGenerator extends ParticleList {
    
    // Properties
    private final Point positionA;
    private final Point positionB;
    private final Coordinate startingDirection;
    private final Coordinate gravityDirection;
    private final float angleVariance;
    private final float speedVariance;
    private final float decayFactor;
    
    private final Random random = new Random();
    
    /**
     * Single particle generator, which can be plugged into a particlePool. Spawns particles from a
     * point or along a line, with a given gravity, dispersion, speed, direction, color, lifetime,
     * and variance.
     *
     * @param maxParticles      maximum number of particles this generator can display
     * @param spawnPositionA    start of spawn line
     * @param spawnPositionB    end of spawn line
     * @param startingDirection initial speed vector
     * @param gravityDirection  gravity vector
     * @param speedVariance     variation in starting speed
     * @param angleVariance     variation in starting angle
     * @param decayFactor       factor determining decay speed/screen time
     */
    public ParticleGenerator(int maxParticles, Point spawnPositionA, Point spawnPositionB,
                             Coordinate startingDirection, Coordinate gravityDirection,
                             float speedVariance, float angleVariance, float decayFactor) {
        super(maxParticles);
        this.positionA = spawnPositionA;
        this.positionB = spawnPositionB;
        this.startingDirection = startingDirection;
        this.gravityDirection = gravityDirection;
        this.angleVariance = angleVariance;
        this.speedVariance = speedVariance;
        this.decayFactor = decayFactor;
    }
    
    /**
     * Adds new particles to the list. Parameters are auto-generated based on the contructor
     * arguments
     *
     * @param count       number of particles to add
     * @param currentTime spawning time of the particles. Used when calculating the current state
     */
    public void addParticles(int count, float currentTime) {
        // Spawn some new particles
        for (int i = 0; i < count; i++) {
            // Random location on a straight line between two spawn points
            float weighting = random.nextFloat();
            
            Coordinate newParticleStartingPoint = new Coordinate(positionB.location);
            newParticleStartingPoint.subtract(positionA.location);
            newParticleStartingPoint.multiply(weighting);
            newParticleStartingPoint.add(positionA.location);
            
            PointColor newParticleColor = positionA.color.mixWithColor(positionB.color, weighting);
            
            // Calculate the velocity vector for this particle
            Coordinate newParticleDirection = new Coordinate(startingDirection);
            final float speedAdjustment = 1.0f + random.nextFloat() * speedVariance;
            newParticleDirection.multiply(speedAdjustment);
            final double angleAdjustment = (random.nextFloat() - 0.5f) * angleVariance;
            newParticleDirection.rotateAroundCoordinate(angleAdjustment, newParticleStartingPoint);
            
            // Update the vertex data
            addNewParticleToFloatArray(newParticleStartingPoint, newParticleColor,
                    newParticleDirection, gravityDirection, decayFactor, currentTime);
        }
    }
}
