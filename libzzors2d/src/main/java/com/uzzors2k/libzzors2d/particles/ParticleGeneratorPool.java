package com.uzzors2k.libzzors2d.particles;

import com.uzzors2k.libzzors2d.DrawablePool;

import static com.uzzors2k.libzzors2d.utils.Constants.BYTES_PER_FLOAT;

/**
 * Drawable pool for particle generators
 */

public class ParticleGeneratorPool extends DrawablePool<ParticleList, ParticleShaderProgram> {
    private static final int STRIDE = ParticleGenerator.TOTAL_COMPONENT_COUNT * BYTES_PER_FLOAT;
    
    @Override
    protected void attachShader(ParticleShaderProgram program) {
        int dataOffset = 0;
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getPositionAttributeLocation(),
                ParticleGenerator.POSITION_COMPONENT_COUNT, STRIDE);
        dataOffset += ParticleGenerator.POSITION_COMPONENT_COUNT;
        
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getColorAttributeLocation(),
                ParticleGenerator.COLOR_COMPONENT_COUNT, STRIDE);
        dataOffset += ParticleGenerator.COLOR_COMPONENT_COUNT;
        
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getDirectionVectorAttributeLocation(),
                ParticleGenerator.VECTOR_COMPONENT_COUNT, STRIDE);
        dataOffset += ParticleGenerator.VECTOR_COMPONENT_COUNT;
        
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getGravityVectorAttributeLocation(),
                ParticleGenerator.GRAVITY_VECTOR_COMPONENT_COUNT, STRIDE);
        dataOffset += ParticleGenerator.GRAVITY_VECTOR_COMPONENT_COUNT;
        
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getDecayFactorAttributeLocation(),
                ParticleGenerator.PARTICLE_DECAY_FACTOR_COMPONENT_COUNT, STRIDE);
        dataOffset += ParticleGenerator.PARTICLE_DECAY_FACTOR_COMPONENT_COUNT;
        
        vertexArray.setVertexAttribPointer(dataOffset,
                program.getParticleStartTimeAttributeLocation(),
                ParticleGenerator.PARTICLE_START_TIME_COMPONENT_COUNT, STRIDE);
    }

    @Override
    protected void performPreDrawEvents(ParticleShaderProgram program, ParticleList drawable) {
        // No special actions required
    }
}
