package com.uzzors2k.libzzors2d.particles;

import com.uzzors2k.libzzors2d.SimpleDrawable;
import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import java.util.Arrays;

import static android.opengl.GLES20.GL_POINTS;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Single particle list, which can be plugged into a particlePool. Base class
 * <p>
 * Particles are created with an initial state, and the GPU calculates where and what
 * they are at the current time. Once enough time elapses the particles will just alpha
 * away into nothing, but still exist. Hence the upper limit MAX_PARTICLE_COUNT
 * In other words, communication is one way. Particles are created with an initial state
 * here, and realized in the GPU
 */

class ParticleList extends SimpleDrawable {
    private final int MAX_PARTICLE_COUNT;
    
    static final int POSITION_COMPONENT_COUNT = 2;
    static final int COLOR_COMPONENT_COUNT = 3;
    static final int VECTOR_COMPONENT_COUNT = 2;
    static final int GRAVITY_VECTOR_COMPONENT_COUNT = 2;
    static final int PARTICLE_DECAY_FACTOR_COMPONENT_COUNT = 1;
    static final int PARTICLE_START_TIME_COMPONENT_COUNT = 1;
    
    static final int TOTAL_COMPONENT_COUNT =
            POSITION_COMPONENT_COUNT
                    + COLOR_COMPONENT_COUNT
                    + VECTOR_COMPONENT_COUNT
                    + GRAVITY_VECTOR_COMPONENT_COUNT
                    + PARTICLE_DECAY_FACTOR_COMPONENT_COUNT
                    + PARTICLE_START_TIME_COMPONENT_COUNT;
    
    // Drawing items
    private int bufferOffset;
    private int nextParticle;
    
    // Vertex data
    private final float[] particles;
    private int currentParticleCount; // Prevents drawing uninitialized particles in particles
    
    /**
     * Base class for particle effects. Holds a list of particles, which are individually
     * simulated and drawn by the GPU
     *
     * @param maxParticles maximum number of particles this generator can display
     */
    ParticleList(int maxParticles) {
        MAX_PARTICLE_COUNT = maxParticles;
        particles = new float[MAX_PARTICLE_COUNT * TOTAL_COMPONENT_COUNT];
        currentParticleCount = 0;
    }
    
    /**
     * Add a single particle to the particles list for drawing/simulation
     *
     * @param position          particle starting location
     * @param color             particle starting color
     * @param direction         particle initial speed vector
     * @param gravity           gravity vector
     * @param decayFactor       factor determining decay speed/screen time
     * @param particleStartTime spawning time of the particles. Used when calculating the current state
     */
    void addNewParticleToFloatArray(Coordinate position, PointColor color, Coordinate direction,
                                    Coordinate gravity, float decayFactor,
                                    float particleStartTime) {
        int currentOffset = nextParticle * TOTAL_COMPONENT_COUNT;
        
        particles[currentOffset++] = position.x;
        particles[currentOffset++] = position.y;
        
        particles[currentOffset++] = color.getRedComponent();
        particles[currentOffset++] = color.getGreenComponent();
        particles[currentOffset++] = color.getBlueComponent();
        
        particles[currentOffset++] = direction.x;
        particles[currentOffset++] = direction.y;
        
        particles[currentOffset++] = gravity.x;
        particles[currentOffset++] = gravity.y;
        
        particles[currentOffset++] = decayFactor;
        
        particles[currentOffset] = particleStartTime;
        
        // Ring buffer of constant size
        nextParticle++;
        if (nextParticle == MAX_PARTICLE_COUNT) {
            nextParticle = 0;
        }
        
        // Keep track of valid data
        if (currentParticleCount < MAX_PARTICLE_COUNT) {
            currentParticleCount++;
        }
    }
    
    @Override
    public int getVertexSize() {
        return currentParticleCount;
    }
    
    @Override
    public float[] getFloatBufferData() {
        if (currentParticleCount < MAX_PARTICLE_COUNT) {
            return Arrays.copyOfRange(particles, 0, currentParticleCount * TOTAL_COMPONENT_COUNT);
        } else {
            return particles;
        }
    }
    
    @Override
    public void setBufferIndex(int index) {
        bufferOffset = index;
    }
    
    @Override
    protected void draw() {
        glDrawArrays(GL_POINTS, bufferOffset, currentParticleCount);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof ParticleList)) return false;
        ParticleList other = (ParticleList) obj;
        
        Object[] arr1 = {other.particles};
        Object[] arr2 = {this.particles};
        
        return other.bufferOffset == this.bufferOffset &&
                other.nextParticle == this.nextParticle &&
                other.zLayer == this.zLayer &&
                other.visible == this.visible &&
                Arrays.deepEquals(arr1, arr2) &&
                other.currentParticleCount == this.currentParticleCount;
    }
}
