package com.uzzors2k.libzzors2d.particles;

import com.uzzors2k.libzzors2d.R;
import com.uzzors2k.libzzors2d.ShaderProgram;

import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniformMatrix4fv;

import android.content.Context;

public class ParticleShaderProgram extends ShaderProgram {
    
    // Uniform constants
    private static final String U_MATRIX = "u_Matrix";
    private static final String U_TIME = "u_Time";
    
    // Attribute constants
    private static final String A_POSITION = "a_Position";
    private static final String A_COLOR = "a_Color";
    private static final String A_DIRECTION_VECTOR = "a_DirectionVector";
    private static final String A_GRAVITY_VECTOR = "a_GravityVector";
    private static final String A_DECAY_FACTOR = "a_DecayFactor";
    private static final String A_PARTICLE_START_TIME = "a_ParticleStartTime";
    
    // Uniform locations
    private final int uMatrixLocation;
    private final int uTimeLocation;
    
    // Attribute locations
    private final int aPositionLocation;
    private final int aColorLocation;
    private final int aDirectionVectorLocation;
    private final int aGravityDirectionVectorLocation;
    private final int aDecayFactorLocation;
    private final int aParticleStartTimeLocation;
    
    /**
     * Load the vertex and fragment shader programs from memory, and set up attribute and uniform
     * locations.
     *
     * @param context current resources context
     */
    public ParticleShaderProgram(Context context) {
        super(context, R.raw.particle_vertex_shader,
                R.raw.particle_fragment_shader);
        
        // Retrieve uniform locations for the shader program.
        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);
        uTimeLocation = glGetUniformLocation(program, U_TIME);
        
        // Retrieve attribute locations for the shader program.
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        aColorLocation = glGetAttribLocation(program, A_COLOR);
        aDirectionVectorLocation = glGetAttribLocation(program, A_DIRECTION_VECTOR);
        aGravityDirectionVectorLocation = glGetAttribLocation(program, A_GRAVITY_VECTOR);
        aDecayFactorLocation = glGetAttribLocation(program, A_DECAY_FACTOR);
        aParticleStartTimeLocation =
                glGetAttribLocation(program, A_PARTICLE_START_TIME);
    }
    
    /**
     * Pass uniform data from CPU to GPU
     *
     * @param matrix      projection matrix
     * @param elapsedTime time elapsed since drawing on the screen started
     */
    public void setUniforms(float[] matrix, float elapsedTime) {
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
        glUniform1f(uTimeLocation, elapsedTime);
    }
    
    @Override
    public int getPositionAttributeLocation() {
        return aPositionLocation;
    }
    
    int getColorAttributeLocation() {
        return aColorLocation;
    }
    
    int getDirectionVectorAttributeLocation() {
        return aDirectionVectorLocation;
    }
    
    int getGravityVectorAttributeLocation() {
        return aGravityDirectionVectorLocation;
    }
    
    int getDecayFactorAttributeLocation() {
        return aDecayFactorLocation;
    }
    
    int getParticleStartTimeAttributeLocation() {
        return aParticleStartTimeLocation;
    }
}
