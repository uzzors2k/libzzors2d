package com.uzzors2k.libzzors2d.particles;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

/**
 * Allows for manually adding particles, for more complex systems where each
 * particle can have individual properties.
 */

public class SimpleParticleGenerator extends ParticleList {
    
    /**
     * Single particle generator, which can be plugged into a particlePool. Allows fully
     * independent and customizable particles to be added.
     *
     * @param maxParticles maximum number of particles this generator can display
     */
    public SimpleParticleGenerator(int maxParticles) {
        super(maxParticles);
    }
    
    /**
     * Add a single particle to the particles list for drawing/simulation
     *
     * @param startPosition     particle starting location
     * @param color             particle starting color
     * @param direction         particle initial speed vector
     * @param gravity           gravity vector
     * @param decayFactor       factor determining decay speed/screen time
     * @param particleStartTime spawning time of the particles. Used when calculating the current state
     */
    public void addParticle(Coordinate startPosition, PointColor color, Coordinate direction,
                            Coordinate gravity, float decayFactor, float particleStartTime) {
        
        // Simply pass these on. This class only exists to hide this function in ParticleGenerator
        addNewParticleToFloatArray(startPosition, color, direction, gravity, decayFactor,
                particleStartTime);
    }
}
