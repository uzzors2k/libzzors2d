package com.uzzors2k.libzzors2d.shapes;

import android.content.Context;

import com.uzzors2k.libzzors2d.R;
import com.uzzors2k.libzzors2d.ShaderProgram;

import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;

/***
 * Excerpted from "OpenGL ES for Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/kbogla for more book information.
 ***/
public class ColorShaderProgram extends ShaderProgram {
    private static final String A_COLOR = "a_Color";
    
    // Attribute locations
    private final int aPositionLocation;
    private final int aColorLocation;
    
    public ColorShaderProgram(Context context) {
        super(context, R.raw.simple_vertex_shader,
                R.raw.simple_fragment_shader);
        // Retrieve attribute locations for the shader program.
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        aColorLocation = glGetAttribLocation(program, A_COLOR);
    }
    
    /**
     * Pass the matrix from the CPU into the shader program
     *
     * @param matrix projection matrix
     */
    public void setUniforms(float[] matrix) {
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
    }
    
    @Override
    public int getPositionAttributeLocation() {
        return aPositionLocation;
    }
    
    int getColorAttributeLocation() {
        return aColorLocation;
    }
}
