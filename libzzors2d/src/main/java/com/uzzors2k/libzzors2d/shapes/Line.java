package com.uzzors2k.libzzors2d.shapes;

import com.uzzors2k.libzzors2d.SimpleDrawable;

import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glLineWidth;

/**
 * Draw a line between two points, with independent color for each point
 */
public class Line extends SimpleDrawable {
    public Point start;
    public Point end;
    private float lineWidth;
    private int bufferOffset;
    
    /**
     * Line object, consisting of two points and a width
     *
     * @param start         starting point of the line
     * @param end           ending point of the line
     * @param lineThickness line thickness
     */
    public Line(Point start, Point end, float lineThickness) {
        this.start = start;
        this.end = end;
        lineWidth = lineThickness;
        bufferOffset = 0;
    }
    
    /**
     * Sets the line width to use when drawing
     *
     * @param newLineWidth line width to draw this line with
     */
    public void setLineWidthInPixels(float newLineWidth) {
        if (newLineWidth < 1.0) {
            lineWidth = 1.0f;
        } else {
            lineWidth = newLineWidth;
        }
    }
    
    @Override
    public float[] getFloatBufferData() {
        float[] startBuffer = start.getFloatBufferData();
        float[] endBuffer = end.getFloatBufferData();
        
        float[] bufferData = new float[startBuffer.length + endBuffer.length];
        
        System.arraycopy(startBuffer, 0, bufferData, 0, startBuffer.length);
        System.arraycopy(endBuffer, 0, bufferData, startBuffer.length, endBuffer.length);
        
        return bufferData;
    }
    
    @Override
    public void setBufferIndex(int index) {
        bufferOffset = index;
    }
    
    @Override
    protected void draw() {
        glLineWidth(lineWidth);
        glDrawArrays(GL_LINES, bufferOffset, 2);
    }
    
    @Override
    public int getVertexSize() {
        return 2;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof Line)) return false;
        Line other = (Line) obj;
        
        return other.start.equals(this.start) &&
                other.end.equals(this.end) &&
                other.lineWidth == this.lineWidth &&
                other.zLayer == this.zLayer &&
                other.visible == this.visible &&
                other.bufferOffset == this.bufferOffset;
    }
}
