package com.uzzors2k.libzzors2d.shapes;

import com.uzzors2k.libzzors2d.SimpleDrawable;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import static android.opengl.GLES20.GL_POINTS;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Draw a single point, with a color
 */
public class Point extends SimpleDrawable {
    public Coordinate location;
    public PointColor color;
    private int bufferOffset;
    
    /**
     * Single point, which consists of a location and color
     *
     * @param position location of the point
     * @param color    color of the point
     */
    public Point(Coordinate position, PointColor color) {
        location = position;
        this.color = color;
        bufferOffset = 0;
    }
    
    @Override
    public float[] getFloatBufferData() {
        float[] colorBuffer = color.getFloatBufferData();
        float[] bufferData = new float[2 + colorBuffer.length];
        
        bufferData[0] = location.x;
        bufferData[1] = location.y;
        System.arraycopy(colorBuffer, 0, bufferData, 2, colorBuffer.length);
        
        return bufferData;
    }
    
    @Override
    public void setBufferIndex(int index) {
        bufferOffset = index;
    }
    
    @Override
    protected void draw() {
        glDrawArrays(GL_POINTS, bufferOffset, 1);
    }
    
    @Override
    public int getVertexSize() {
        return 1;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof Point)) return false;
        Point other = (Point) obj;
        
        return other.location.equals(this.location) &&
                other.color.equals(this.color) &&
                other.zLayer == this.zLayer &&
                other.visible == this.visible &&
                other.bufferOffset == this.bufferOffset;
    }
}
