package com.uzzors2k.libzzors2d.shapes;

import static java.lang.Math.abs;

/**
 * Simple data type, color
 */
public class PointColor {
    private final float red;
    private final float green;
    private final float blue;
    private final float alpha;
    
    /**
     * Create a simple Red, Green, Blue color object. Values outside the valid range are
     * clamped/saturated.
     *
     * @param red   red color, from 0.0 to 1.0
     * @param green green color, from 0.0 to 1.0
     * @param blue  blue color, from 0.0 to 1.0
     * @param alpha transparency, from 0.0 to 1.0
     */
    public PointColor(float red, float green, float blue, float alpha) {
        this.red = Math.min(Math.max(red, 0.0f), 1.0f);
        this.green = Math.min(Math.max(green, 0.0f), 1.0f);
        this.blue = Math.min(Math.max(blue, 0.0f), 1.0f);
        this.alpha = Math.min(Math.max(alpha, 0.0f), 1.0f);
    }

    static public PointColor createFromAndroidColor(int color) {
        float redComponent = ((color >> 16) & 0xff) / 255.0f;
        float greenComponent = ((color >> 8) & 0xff) / 255.0f;
        float blueComponent = (color & 0xff) / 255.0f;
        float alphaComponent = ((color >> 24) & 0xff) / 255.0f;
        return new PointColor(redComponent, greenComponent, blueComponent, alphaComponent);
    }
    
    /**
     * @return a float array containing all color data which will be passed from CPU to GPU
     */
    public float[] getFloatBufferData() {
        return new float[]{red, green, blue, alpha};
    }
    
    public float getRedComponent() {
        return red;
    }
    
    public float getGreenComponent() {
        return green;
    }
    
    public float getBlueComponent() {
        return blue;
    }
    
    public float getAlphaComponent() {
        return alpha;
    }
    
    /**
     * Create a new color which is a mix of this color, and otherColor
     *
     * @param otherColor other color to mix with
     * @param amount     factor of the difference between this color and other color to add to the
     *                   current color
     * @return the resulting color
     */
    public PointColor mixWithColor(PointColor otherColor, float amount) {
        float mixRed = amount * (otherColor.getRedComponent() - red) + red;
        float mixGreen = amount * (otherColor.getGreenComponent() - green) + green;
        float mixBlue = amount * (otherColor.getBlueComponent() - blue) + blue;
        float mixAlpha = amount * (otherColor.getAlphaComponent() - alpha) + alpha;
        
        return new PointColor(abs(mixRed), abs(mixGreen), abs(mixBlue), abs(mixAlpha));
    }
    
    public int toAndroidColorInt() {
        return ((int) (alpha * 255) & 0xff) << 24 |
                ((int) (red * 255) & 0xff) << 16 |
                ((int) (green * 255) & 0xff) << 8 |
                ((int) (blue * 255) & 0xff);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof PointColor)) return false;
        PointColor other = (PointColor) obj;
        
        return other.red == this.red &&
                other.green == this.green &&
                other.blue == this.blue &&
                other.alpha == this.alpha;
    }

    public PointColor deepCopy() {
        return new PointColor(red, green, blue, alpha);
    }
}
