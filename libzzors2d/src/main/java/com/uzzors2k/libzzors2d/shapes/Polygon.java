package com.uzzors2k.libzzors2d.shapes;

import com.uzzors2k.libzzors2d.SimpleDrawable;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.VertexArray;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Simple class for drawing polygons, with independent color for each point
 */
public class Polygon extends SimpleDrawable {
    private final ArrayList<Point> points;
    private int bufferOffset;
    
    /**
     * Polygon, which can have unlimited points. Points are drawn in the order added.
     *
     */
    public Polygon() {
        points = new ArrayList<>();
        bufferOffset = 0;
    }
    
    /**
     * Translate the entire polygon by a the distance given
     *
     * @param offset distance to move the polygon
     */
    public void offsetEntirePolygon(Coordinate offset) {
        for (Point singlePoint : points) {
            singlePoint.location.add(offset);
        }
    }
    
    /**
     * Add a new point to the polygon
     *
     * @param newPoint new point in the polygon
     */
    public void addPolygonPoint(Point newPoint) {
        points.add(newPoint);
    }
    
    public void removePoint(int index) {
        points.remove(index);
    }
    
    public void removePoint(Point point) {
        points.remove(point);
    }

    @Override
    public float[] getFloatBufferData() {
        float[] bufferData = new float[0];
        
        for (Point point : points) {
            float[] pointBufferContents = point.getFloatBufferData();
            bufferData = VertexArray.concatFloatArrays(bufferData, pointBufferContents);
        }
        
        return bufferData;
    }
    
    @Override
    public void setBufferIndex(int index) {
        bufferOffset = index;
    }
    
    @Override
    protected void draw() {
        if (points.size() < 3) {
            throw new IllegalArgumentException("Not enough points for a polygon!");
        }
        glDrawArrays(GL_TRIANGLE_FAN, bufferOffset, points.size());
    }
    
    @Override
    public int getVertexSize() {
        return points.size();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof Polygon)) return false;
        Polygon other = (Polygon) obj;
        
        Set<Point> set1 = new HashSet<>(other.points);
        for (Point point : this.points) {
            if (!set1.contains(point)) {
                return false;
            }
        }
        
        return other.zLayer == this.zLayer &&
                other.visible == this.visible &&
                other.bufferOffset == this.bufferOffset;
    }
}
