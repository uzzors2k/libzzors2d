package com.uzzors2k.libzzors2d.shapes;

import com.uzzors2k.libzzors2d.DrawablePool;
import com.uzzors2k.libzzors2d.SimpleDrawable;

import static com.uzzors2k.libzzors2d.utils.Constants.BYTES_PER_FLOAT;

/***
 * Object which contains several simple geometric shapes,
 * which can be drawn directly to the screen.
 ***/
public class ShapesPool extends DrawablePool<SimpleDrawable, ColorShaderProgram> {
    private static final int COLOR_COMPONENT_COUNT = 4;
    private static final int STRIDE = (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) *
            BYTES_PER_FLOAT;
    
    @Override
    protected void attachShader(ColorShaderProgram program) {
        vertexArray.setVertexAttribPointer(
                0,
                program.getPositionAttributeLocation(),
                POSITION_COMPONENT_COUNT,
                STRIDE);
        vertexArray.setVertexAttribPointer(
                POSITION_COMPONENT_COUNT,
                program.getColorAttributeLocation(),
                COLOR_COMPONENT_COUNT,
                STRIDE);
    }

    @Override
    protected void performPreDrawEvents(ColorShaderProgram program, SimpleDrawable drawable) {
        // No special actions required
    }
}
