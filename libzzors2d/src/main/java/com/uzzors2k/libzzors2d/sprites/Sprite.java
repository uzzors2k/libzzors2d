package com.uzzors2k.libzzors2d.sprites;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.SimpleDrawable;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Sprite class. Uses sections of a bitmap atlas, defined by
 * cells.
 */
public class Sprite extends SimpleDrawable {
    
    // Cell size is the number of atlas cells per sprite frame, default 1x1
    private final int CELL_SIZE_X;
    private final int CELL_SIZE_Y;
    // Starting location of sprite frames, in atlas cell indices
    private final int CELL_OFFSET_X;
    private final int CELL_OFFSET_Y;
    // Frame count, i.e. frames in the animation
    private final int FRAME_COUNT_X;
    private final int FRAME_COUNT_Y;
    
    // Animation related variables
    private int currentFrameIndex;
    public final int LAST_FRAME;
    // Based on the cell size used by the bitmap atlas
    private Coordinate incrementSize;
    private final Coordinate HALF_PIXEL_SIZE;
    // Vertex data
    private float[] spriteRectangle;
    
    private RotatableBox atlasLocation;
    private RotatableBox screenLocation;
    private int bufferOffset;

    private PointColor filterColorBottomLeft;
    private PointColor filterColorBottomRight;
    private PointColor filterColorTopLeft;
    private PointColor filterColorTopRight;

    // 0 - default, 1 - filter, 2 and up is bitmask. 2 - replace red, 4 - replace green, 8 - replace blue
    private int coloringMode;
    private PointColor redReplacement;
    private PointColor greenReplacement;
    private PointColor blueReplacement;

    public boolean automaticVerticeUpdates = true;

    /**
     * Sprite object, which draws textures cut from a bitmap atlas.
     *
     * @param startCellX - Starting offset in cells, where in the atlas to find this sprite's
     *                  frames.
     * @param startCellY - Starting offset in cells, where in the atlas to find this sprite's
     *                  frames.
     * @param cellLumpingX - How many cells to use in one sprite frame
     * @param cellLumpingY - How many cells to use in one sprite frame
     * @param frameCountX - Number of frame columns
     * @param frameCountY - Number of frame rows
     * @param halfPixelSize - Set the size of half a pixel. Use this to fix texture bleeding
     *                      problems when not having an empty border in the sprite atlas.
     */
    public Sprite(int startCellX, int startCellY, int cellLumpingX, int cellLumpingY, int
            frameCountX, int frameCountY, Coordinate halfPixelSize) {
        super();
        
        CELL_OFFSET_X = startCellX;
        CELL_OFFSET_Y = startCellY;
        CELL_SIZE_X = cellLumpingX;
        CELL_SIZE_Y = cellLumpingY;
        FRAME_COUNT_X = frameCountX;
        FRAME_COUNT_Y = frameCountY;
        
        LAST_FRAME = (FRAME_COUNT_X * FRAME_COUNT_Y) - 1;

        bufferOffset = 0;
        currentFrameIndex = 0;
        zLayer = 0;
        incrementSize = new Coordinate(0, 0);
        HALF_PIXEL_SIZE = halfPixelSize;
        screenLocation = new RotatableBox();
        // Set a dummy size. Forgetting to set a size has caused so many difficult bugs
        screenLocation.setSize(new Coordinate(1.0f, 1.0f));

        filterColorBottomLeft = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorBottomRight = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorTopLeft = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorTopRight = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);

        redReplacement = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        greenReplacement = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        blueReplacement = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);

        coloringMode = 0;

        updateFrameBox();
        updateVerticeData();
    }
    
    @Override
    public float[] getFloatBufferData() {
        return spriteRectangle;
    }
    
    @Override
    public void setBufferIndex(int index) {
        bufferOffset = index;
    }
    
    @Override
    protected void draw() {
        glDrawArrays(GL_TRIANGLE_STRIP, bufferOffset, 4);
    }
    
    @Override
    public int getVertexSize() {
        return 4;
    }

    public int getColoringMode() {
        return coloringMode;
    }

    public PointColor getRedReplacementColor() {
        return redReplacement;
    }

    public PointColor getGreenReplacementColor() {
        return greenReplacement;
    }

    public PointColor getBlueReplacementColor() {
        return blueReplacement;
    }

    public boolean isRedBeingReplaced() {
        return (coloringMode & 2) > 0;
    }

    public boolean isGreenBeingReplaced() {
        return (coloringMode & 4) > 0;
    }

    public boolean isBlueBeingReplaced() {
        return (coloringMode & 8) > 0;
    }

    /**
     * Set the current frame
     *
     * @param frameNumber frame to display from the bitmap atlas
     */
    public void setCurrentFrame(int frameNumber) {
        currentFrameIndex = Math.min(LAST_FRAME, Math.max(frameNumber, 0));
        updateFrameBox();
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    /**
     * Update the cell size used by the bitmap atlas
     *
     * @param cellSize smallest cell size in the bitmap atlas
     */
    public void setAtlasCellSize(Coordinate cellSize) {
        incrementSize = cellSize;
        updateFrameBox();
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }

    /**
     * Update the float array passed to the GPU
     */
    public void updateVerticeData() {
        
        // Screen, Coordinate space from [(-1,-1) -> (1,1)]
        Coordinate screen_tl = screenLocation.TopLeft;
        Coordinate screen_tr = screenLocation.TopRight;
        Coordinate screen_br = screenLocation.BottomRight;
        Coordinate screen_bl = screenLocation.BottomLeft;
        
        // Bitmap atlas, Coordinate space from [(0,0) -> (1,1)]
        Coordinate atlas_tl = atlasLocation.TopLeft.deepCopy();
        Coordinate atlas_tr = atlasLocation.TopRight.deepCopy();
        Coordinate atlas_br = atlasLocation.BottomRight.deepCopy();
        Coordinate atlas_bl = atlasLocation.BottomLeft.deepCopy();

        // Shrink the bitmap atlas location by half a pixel, to avoid texture bleeding
        float halfPixelX = HALF_PIXEL_SIZE.x;
        float halfPixelY = HALF_PIXEL_SIZE.y;

        atlas_tl.x += halfPixelX;
        atlas_tl.y -= halfPixelY;

        atlas_tr.x -= halfPixelX;
        atlas_tr.y -= halfPixelY;

        atlas_bl.x += halfPixelX;
        atlas_bl.y += halfPixelY;

        atlas_br.x -= halfPixelX;
        atlas_br.y += halfPixelY;

        spriteRectangle = new float[]{
                // Order of coordinates: X, Y, S, T, ColorRed, ColorGreen, ColorBlue, Alpha
                // X,Y are screen placement coordinates,
                // S,T and texture coordinates.

                screen_bl.x, screen_bl.y, atlas_tl.x, atlas_tl.y, filterColorBottomLeft.getRedComponent(), filterColorBottomLeft.getGreenComponent(), filterColorBottomLeft.getBlueComponent(), filterColorBottomLeft.getAlphaComponent(),
                screen_tl.x, screen_tl.y, atlas_bl.x, atlas_bl.y, filterColorTopLeft.getRedComponent(), filterColorTopLeft.getGreenComponent(), filterColorTopLeft.getBlueComponent(), filterColorTopLeft.getAlphaComponent(),
                screen_br.x, screen_br.y, atlas_tr.x, atlas_tr.y, filterColorBottomRight.getRedComponent(), filterColorBottomRight.getGreenComponent(), filterColorBottomRight.getBlueComponent(), filterColorBottomRight.getAlphaComponent(),
                screen_tr.x, screen_tr.y, atlas_br.x, atlas_br.y, filterColorTopRight.getRedComponent(), filterColorTopRight.getGreenComponent(), filterColorTopRight.getBlueComponent(), filterColorTopRight.getAlphaComponent(),
        };
    }
    
    /**
     * Recreates the box used to cut texture from the bitmap atlas
     */
    private void updateFrameBox() {
        Coordinate frameSize = new Coordinate(
                incrementSize.x * CELL_SIZE_X,
                incrementSize.y * CELL_SIZE_Y);

        int xColumn = CELL_OFFSET_X + (currentFrameIndex % FRAME_COUNT_X);
        int yRow = CELL_OFFSET_Y + (currentFrameIndex / FRAME_COUNT_X);

        Coordinate startOffset = new Coordinate(
                frameSize.x * xColumn,
                (frameSize.y * yRow) + frameSize.y);

        atlasLocation = new RotatableBox(startOffset, frameSize);
    }
    
    /**
     * Set the position, in relation to the alignment given
     *
     * @param position  coordinate which will be set as the given alignment of the box
     * @param alignment which part of the box the position will be
     */
    public void setPosition(Coordinate position, RotatableBox.Alignment alignment) {
        screenLocation.setPosition(position, alignment);
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    /**
     * Set the size of the sprite on the screen
     *
     * @param size size of the sprite on the screen
     */
    public void setSpriteSize(Coordinate size) {
        screenLocation.setSize(size);
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    /**
     * Mirror/flip the sprite around a given axis
     *
     * @param horizontally mirror horizontally
     * @param vertically   mirror vertically
     */
    public void setSpriteMirroring(boolean horizontally, boolean vertically) {
        screenLocation.setMirroring(horizontally, vertically);
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    /**
     * Rotate the sprite on screen with the given angle
     *
     * @param radiansAngle angle in radians to rotate
     */
    public void setSpriteRotationInRadians(double radiansAngle) {
        screenLocation.setRotation(radiansAngle);
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    /**
     * Simply step forward one frame, and wrap around to the end when complete
     */
    public void stepCurrentFrame() {
        currentFrameIndex++;
        if (currentFrameIndex > LAST_FRAME) {
            currentFrameIndex = 0;
        }
        updateFrameBox();
        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }

    /**
     * Filters rendering of the sprite with the specified color filter. Define all colors at maximum
     * (1.0f) for neutral filtering, and all at minimum (0.0f) for an all-black sprite. Colors will
     * create a gradient if different
     * @param topLeftCorner Color to filter from the top left corner.
     * @param topRightCorner Color to filter from the top right corner.
     * @param bottomLeftCorner Color to filter from the bottom left corner.
     * @param bottomRightCorner Color to filter from the bottom right corner.
     */
    public void setColorFilter(PointColor topLeftCorner,
                         PointColor topRightCorner,
                         PointColor bottomLeftCorner,
                         PointColor bottomRightCorner) {
        filterColorBottomLeft = bottomLeftCorner;
        filterColorBottomRight = bottomRightCorner;
        filterColorTopLeft = topLeftCorner;
        filterColorTopRight =  topRightCorner;

        coloringMode = 1;

        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }

    public void resetColoringMode() {
        coloringMode = 0;
    }

    /**
     * Replace the given shade with a different color.
     * @param replacementColor Color used when replacing certain shades
     */
    public void setRedReplacement(PointColor replacementColor) {
        if ((coloringMode & 1) == 1) {
            coloringMode &= ~1;
        }

        redReplacement = replacementColor;
        coloringMode |= 2;
    }

    /**
     * Replace the given shade with a different color.
     * @param replacementColor Color used when replacing certain shades
     */
    public void setGreenReplacement(PointColor replacementColor) {
        if ((coloringMode & 1) == 1) {
            coloringMode &= ~1;
        }

        greenReplacement = replacementColor;
        coloringMode |= 4;
    }

    /**
     * Replace the given shade with a different color.
     * @param replacementColor Color used when replacing certain shades
     */
    public void setBlueReplacement(PointColor replacementColor) {
        if ((coloringMode & 1) == 1) {
            coloringMode &= ~1;
        }

        blueReplacement = replacementColor;
        coloringMode |= 8;
    }

    /**
     * Filters rendering of the sprite with the specified color filter. Define all colors at maximum
     * (1.0f) for neutral filtering, and all at minimum (0.0f) for an all-black sprite.
     * @param uniformColorFilter Color filter to apply to the entire sprite
     */
    public void setColorFilter(PointColor uniformColorFilter) {
        filterColorBottomLeft = uniformColorFilter;
        filterColorBottomRight = uniformColorFilter;
        filterColorTopLeft = uniformColorFilter;
        filterColorTopRight =  uniformColorFilter;

        coloringMode = 1;

        if (automaticVerticeUpdates) {
            updateVerticeData();
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof Sprite)) return false;
        Sprite other = (Sprite) obj;

        return other.CELL_SIZE_X == this.CELL_SIZE_X &&
                other.CELL_SIZE_Y == this.CELL_SIZE_Y &&
                other.CELL_OFFSET_X == this.CELL_OFFSET_X &&
                other.CELL_OFFSET_Y == this.CELL_OFFSET_Y &&
                other.FRAME_COUNT_X == this.FRAME_COUNT_X &&
                other.FRAME_COUNT_Y == this.FRAME_COUNT_Y &&
                other.HALF_PIXEL_SIZE.equals(this.HALF_PIXEL_SIZE) &&
                other.LAST_FRAME == this.LAST_FRAME &&
                other.currentFrameIndex == this.currentFrameIndex &&
                other.zLayer == this.zLayer &&
                other.visible == this.visible &&
                //Arrays.deepEquals(arr1, arr2) &&
                other.bufferOffset == this.bufferOffset &&
                other.incrementSize.equals(this.incrementSize) &&
                other.atlasLocation.equals(this.atlasLocation) &&
                other.screenLocation.equals(this.screenLocation) &&
                other.filterColorBottomLeft.equals(this.filterColorBottomLeft) &&
                other.filterColorBottomRight.equals(this.filterColorBottomRight) &&
                other.filterColorTopLeft.equals(this.filterColorTopLeft) &&
                other.coloringMode == this.coloringMode &&
                other.redReplacement.equals(this.redReplacement) &&
                other.greenReplacement.equals(this.greenReplacement) &&
                other.blueReplacement.equals(this.blueReplacement);
    }

    public Sprite deepCopy() {
        Sprite copy = new Sprite(CELL_OFFSET_X,
                CELL_OFFSET_Y,
                CELL_SIZE_X,
                CELL_SIZE_Y,
                FRAME_COUNT_X,
                FRAME_COUNT_Y,
                HALF_PIXEL_SIZE);

        copy.currentFrameIndex = this.currentFrameIndex;
        copy.zLayer = this.zLayer;
        copy.visible = this.visible;
        //Arrays.deepEquals(arr1, arr2);
        copy.bufferOffset = this.bufferOffset;

        copy.incrementSize = incrementSize.deepCopy();
        copy.atlasLocation = atlasLocation.deepCopy();
        copy.screenLocation = screenLocation.deepCopy();
        copy.filterColorBottomLeft = filterColorBottomLeft.deepCopy();
        copy.filterColorBottomRight = filterColorBottomRight.deepCopy();
        copy.filterColorTopLeft = filterColorTopLeft.deepCopy();
        copy.filterColorTopRight = filterColorTopRight.deepCopy();

        copy.coloringMode = coloringMode;
        copy.redReplacement = redReplacement.deepCopy();
        copy.greenReplacement = greenReplacement.deepCopy();
        copy.blueReplacement = blueReplacement.deepCopy();

        copy.updateFrameBox();
        copy.updateVerticeData();

        return copy;
    }
}
