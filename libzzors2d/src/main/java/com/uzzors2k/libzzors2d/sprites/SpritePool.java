package com.uzzors2k.libzzors2d.sprites;

import com.uzzors2k.libzzors2d.DrawablePool;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import java.util.List;

import static com.uzzors2k.libzzors2d.utils.Constants.BYTES_PER_FLOAT;

/**
 * Bitmap atlas class. Defines cell locations in in a bitmap, which can
 * be used by sprites added to the atlas pool.
 */
public class SpritePool extends DrawablePool<Sprite, TextureShaderProgram> {
    private static final int TEXTURE_COORDINATES_COMPONENT_COUNT = 2;
    private static final int TEXTURE_COLOR_COMPONENT_COUNT = 4;
    private static final int STRIDE = (POSITION_COMPONENT_COUNT +
            TEXTURE_COORDINATES_COMPONENT_COUNT +
            TEXTURE_COLOR_COMPONENT_COUNT) * BYTES_PER_FLOAT;
    private final Coordinate cellSize;
    
    /**
     * Sprite pool which draws sections of a single texture resource, in a bitmap atlas manner
     *
     * @param atlasCellSize smallest size of individual cells in the bitmap atlas
     */
    public SpritePool(Coordinate atlasCellSize) {
        cellSize = atlasCellSize;
    }
    
    /**
     * Sprite pool which draws sections of a single texture resource, in a bitmap atlas manner
     *
     * @param numberOfCellsX number of columns in the bitmap atlas
     * @param numberOfCellsY number of rows in the bitmap atlas
     */
    public SpritePool(int numberOfCellsX, int numberOfCellsY) {
        cellSize = new Coordinate(1.0f / numberOfCellsX, 1.0f / numberOfCellsY);
    }
    
    @Override
    protected void attachShader(TextureShaderProgram program) {
        int dataOffset = 0;
        vertexArray.setVertexAttribPointer(
                dataOffset,
                program.getPositionAttributeLocation(),
                POSITION_COMPONENT_COUNT,
                STRIDE);
        
        dataOffset += POSITION_COMPONENT_COUNT;
        vertexArray.setVertexAttribPointer(
                dataOffset,
                program.getTextureCoordinatesAttributeLocation(),
                TEXTURE_COORDINATES_COMPONENT_COUNT,
                STRIDE);
        
        dataOffset += TEXTURE_COORDINATES_COMPONENT_COUNT;
        vertexArray.setVertexAttribPointer(
                dataOffset,
                program.getTextureColorAttributeLocation(),
                TEXTURE_COLOR_COMPONENT_COUNT,
                STRIDE);
    }
    
    @Override
    public void addDrawable(Sprite newDrawable) {
        newDrawable.setAtlasCellSize(cellSize);
        super.addDrawable(newDrawable);
    }

    @Override
    public void addAllDrawable(List<Sprite> newDrawables) {
        for (Sprite drawable : newDrawables) {
            drawable.setAtlasCellSize(cellSize);
        }
        super.addAllDrawable(newDrawables);
    }

    @Override
    protected void performPreDrawEvents(TextureShaderProgram program, Sprite drawable) {
        program.setTextureColoringMode(drawable.getColoringMode());
        program.setRedReplacementColor(drawable.getRedReplacementColor(), drawable.isRedBeingReplaced());
        program.setGreenReplacementColor(drawable.getGreenReplacementColor(), drawable.isGreenBeingReplaced());
        program.setBlueReplacementColor(drawable.getBlueReplacementColor(), drawable.isBlueBeingReplaced());
    }
}

