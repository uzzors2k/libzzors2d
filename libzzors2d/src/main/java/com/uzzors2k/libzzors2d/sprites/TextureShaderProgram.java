package com.uzzors2k.libzzors2d.sprites;

import android.content.Context;

import com.uzzors2k.libzzors2d.R;
import com.uzzors2k.libzzors2d.ShaderProgram;
import com.uzzors2k.libzzors2d.shapes.PointColor;

import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniform4fv;
import static android.opengl.GLES20.glUniformMatrix4fv;

/***
 * Excerpted from "OpenGL ES for Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/kbogla for more book information.
 ***/
public class TextureShaderProgram extends ShaderProgram {
    // Names of shader program variables
    private static final String U_TEXTURE_UNIT = "u_TextureUnit";
    private static final String U_COLORING_MODE = "u_ColoringMode";
    private static final String U_RED_REPLACEMENT = "u_RedReplacement";
    private static final String U_GREEN_REPLACEMENT = "u_GreenReplacement";
    private static final String U_BLUE_REPLACEMENT = "u_BlueReplacement";
    private static final String U_REPLACE_RED = "u_ReplaceRed";
    private static final String U_REPLACE_GREEN = "u_ReplaceGreen";
    private static final String U_REPLACE_BLUE = "u_ReplaceBlue";

    private static final String A_TEXTURE_COORDINATES = "a_TextureCoordinates";
    private static final String A_TEXTURE_COLOR = "a_TextureColor";

    // Uniform locations
    private final int uTextureUnitLocation;
    private final int uColoringMode;
    private final int uRedReplacement;
    private final int uGreenReplacement;
    private final int uBlueReplacement;
    private final int uReplaceRed;
    private final int uReplaceGreen;
    private final int uReplaceBlue;

    // Attribute locations
    private final int aPositionLocation;
    private final int aTextureCoordinatesLocation;
    private final int aTextureColorLocation;

    public TextureShaderProgram(Context context) {
        super(context, R.raw.texture_vertex_shader,
                R.raw.texture_fragment_shader);
        
        // Retrieve uniform locations for the shader program.
        uTextureUnitLocation = glGetUniformLocation(program, U_TEXTURE_UNIT);
        uColoringMode = glGetUniformLocation(program, U_COLORING_MODE);
        uRedReplacement = glGetUniformLocation(program, U_RED_REPLACEMENT);
        uGreenReplacement = glGetUniformLocation(program, U_GREEN_REPLACEMENT);
        uBlueReplacement = glGetUniformLocation(program, U_BLUE_REPLACEMENT);
        uReplaceRed = glGetUniformLocation(program, U_REPLACE_RED);
        uReplaceGreen = glGetUniformLocation(program, U_REPLACE_GREEN);
        uReplaceBlue = glGetUniformLocation(program, U_REPLACE_BLUE);

                // Retrieve attribute locations for the shader program.
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        aTextureCoordinatesLocation = glGetAttribLocation(program, A_TEXTURE_COORDINATES);
        aTextureColorLocation = glGetAttribLocation(program, A_TEXTURE_COLOR);
    }
    
    /**
     * Move data from CPU to GPU.
     *
     * @param matrix    projection matrix to use
     * @param textureId resource ID for the texture map to draw from. Used for all items drawing
     *                  with this shader program
     */
    public void setUniforms(float[] matrix, int textureId) {
        // Pass the matrix into the shader program.
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
        
        // Set the active texture unit to texture unit 0.
        glActiveTexture(GL_TEXTURE0);
        
        // Bind the texture to this unit.
        glBindTexture(GL_TEXTURE_2D, textureId);
        
        // Tell the texture uniform sampler to use this texture in the shader by
        // telling it to read from texture unit 0.
        glUniform1i(uTextureUnitLocation, 0);
    }
    
    @Override
    public int getPositionAttributeLocation() {
        return aPositionLocation;
    }
    
    int getTextureCoordinatesAttributeLocation() {
        return aTextureCoordinatesLocation;
    }

    int getTextureColorAttributeLocation() {
        return aTextureColorLocation;
    }

    /**
     * Sets the coloring mode to use for the texture.
     * @param coloringMode 0 - Default, 1 - Dynamic color filtering, 2 - Color replacement
     */
    void setTextureColoringMode(int coloringMode) {
        glUniform1i(uColoringMode, coloringMode);
    }

    void setRedReplacementColor(PointColor replacement, boolean enable) {
        glUniform4fv(uRedReplacement, 1, replacement.getFloatBufferData(), 0);
        glUniform1i(uReplaceRed, enable ? 1 : 0);
    }

    void setGreenReplacementColor(PointColor replacement, boolean enable) {
        glUniform4fv(uGreenReplacement, 1, replacement.getFloatBufferData(), 0);
        glUniform1i(uReplaceGreen, enable ? 1 : 0);
    }

    void setBlueReplacementColor(PointColor replacement, boolean enable) {
        glUniform4fv(uBlueReplacement, 1, replacement.getFloatBufferData(), 0);
        glUniform1i(uReplaceBlue, enable ? 1 : 0);
    }
}

