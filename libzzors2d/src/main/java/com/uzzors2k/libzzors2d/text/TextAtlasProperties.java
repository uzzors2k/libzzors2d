package com.uzzors2k.libzzors2d.text;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import java.util.Arrays;

/**
 * Created by uzzors2k on 10.07.2017.
 * Holds data about the bitmap atlas, used when creating textboxes
 */
public class TextAtlasProperties {
    final int COLUMN_COUNT;
    final int ROW_COUNT;
    final float[] CHARACTER_WIDTHS;
    final int CHARACTER_START_INDEX;
    final int CHARACTER_END_INDEX;
    final Coordinate CELL_SIZE;
    final Coordinate HALF_PIXEL_SIZE;
    
    /**
     * Stores a number of properties required when using a bitmap atlas for drawing text
     *
     * @param numberOfCellsX      columns in the bitmap atlas
     * @param numberOfCellsY      rows in the bitmap atlas
     * @param charWidthArray      array containing individual character widths
     * @param characterStartIndex starting index for character
     * @param atlasCellSize       cell size for the bitmap atlas
     */
    public TextAtlasProperties(int numberOfCellsX,
                               int numberOfCellsY,
                               float[] charWidthArray,
                               int characterStartIndex,
                               int characterEndIndex,
                               Coordinate atlasCellSize,
                               Coordinate halfPixelSize) {
        COLUMN_COUNT = numberOfCellsX;
        ROW_COUNT = numberOfCellsY;
        CHARACTER_WIDTHS = charWidthArray;
        CHARACTER_START_INDEX = characterStartIndex;
        CHARACTER_END_INDEX = characterEndIndex;
        CELL_SIZE = atlasCellSize;
        HALF_PIXEL_SIZE = halfPixelSize;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof TextAtlasProperties)) return false;
        TextAtlasProperties other = (TextAtlasProperties) obj;
        
        Object[] arr1 = {other.CHARACTER_WIDTHS};
        Object[] arr2 = {this.CHARACTER_WIDTHS};
        
        return other.COLUMN_COUNT == this.COLUMN_COUNT &&
                other.ROW_COUNT == this.ROW_COUNT &&
                Arrays.deepEquals(arr1, arr2) &&
                other.CELL_SIZE.equals(this.CELL_SIZE) &&
                other.HALF_PIXEL_SIZE.equals(this.HALF_PIXEL_SIZE) &&
                other.CHARACTER_START_INDEX == this.CHARACTER_START_INDEX;
    }

    public String filterOutInvalidCharacters(String text) {
        StringBuilder sanitizedString = new StringBuilder();
        for (int i = 0, n = text.length(); i < n; i++) {
            char c = text.charAt(i);
            if (c < CHARACTER_START_INDEX) {
                c = '?';
            }
            else if (c > CHARACTER_END_INDEX) {
                c = '?';
            }
            sanitizedString.append(c);
        }

        return sanitizedString.toString();
    }
}
