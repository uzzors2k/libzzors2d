package com.uzzors2k.libzzors2d.text;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.sprites.Sprite;
import com.uzzors2k.libzzors2d.sprites.SpritePool;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.libzzors2d.sprites.TextureShaderProgram;

import java.text.Normalizer;

/**
 * Single textbox. Uses a spritepool internally to hold all displayed characters.
 * Can be added to a TextPool for drawing.
 */
public class TextBox {
    private final TextAtlasProperties textAtlasProperties;
    private final SpritePool characterPool;
    
    private String currentText;
    
    // Bounding box related code
    private Coordinate textCharacterSize;
    private RotatableBox textBoundingBox;
    private RotatableBox.Alignment currentAlignment;
    private Coordinate userPosition;
    private double currentRotation;
    
    // Graphics related
    private PointColor filterColorBottomLeft;
    private PointColor filterColorBottomRight;
    private PointColor filterColorTopLeft;
    private PointColor filterColorTopRight;

    public boolean visible;
    
    /**
     * Text box which can be placed, rotated, scaled, and written to
     *
     * @param textProps bitmap atlas properties used when drawing characters
     */
    public TextBox(TextAtlasProperties textProps) {
        super();
        textAtlasProperties = textProps;
        characterPool = new SpritePool(textProps.CELL_SIZE);
        
        currentText = "???";
        currentAlignment = RotatableBox.Alignment.TOP_LEFT;
        textCharacterSize = new Coordinate(0.0f, 0.0f);
        userPosition = new Coordinate(0.0f, 0.0f);
        textBoundingBox = new RotatableBox();
        currentRotation = 0.0;

        filterColorBottomLeft = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorBottomRight = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorTopLeft = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
        filterColorTopRight = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);

        visible = true;
    }
    
    /**
     * Set the text to display.
     * @param text Text to display
     */
    public void setText(String text) {
        // Handle Unicode characters, by converting to an equivalent ASCII character
        String asciiString = Normalizer.normalize(text, Normalizer.Form.NFD);
        asciiString = asciiString.replaceAll("[^\\p{ASCII}]", "");

        currentText = textAtlasProperties.filterOutInvalidCharacters(asciiString);

        updateInternalPosition();
        reDrawTextBox();
    }
    
    /**
     * Set the position, in relation to the alignment given
     *
     * @param position  coordinate which will be set as the given alignment of the box
     * @param alignment which part of the box the position will be
     */
    public void setPosition(Coordinate position, RotatableBox.Alignment alignment) {
        userPosition = position;
        currentAlignment = alignment;
        updateInternalPosition();
        reDrawTextBox();
    }
    
    /**
     * @param size character size
     */
    public void setCharacterSize(Coordinate size) {
        textCharacterSize = size;
        updateInternalPosition();
        reDrawTextBox();
    }
    
    /**
     * @param rotationInRadians radians to rotate the text box
     */
    public void setTextBoxRotationRadians(double rotationInRadians) {
        currentRotation = rotationInRadians;
        updateInternalPosition();
        reDrawTextBox();
    }

    /**
     * Filters rendering of the sprite with the specified color filter. Define all colors at maximum
     * (1.0f) for neutral filtering, and all at minimum (0.0f) for an all-black sprite.
     * @param topLeftCorner Color filter for top left corner
     * @param topRightCorner Color filter for top right corner
     * @param bottomLeftCorner Color filter for bottom left corner
     * @param bottomRightCorner Color filter for bottom right corner
     */
    public void setColorFilter(PointColor topLeftCorner,
                               PointColor topRightCorner,
                               PointColor bottomLeftCorner,
                               PointColor bottomRightCorner) {
        filterColorBottomLeft = bottomLeftCorner;
        filterColorBottomRight = bottomRightCorner;
        filterColorTopLeft = topLeftCorner;
        filterColorTopRight =  topRightCorner;

        reDrawTextBox();
    }

    /**
     * Filters rendering of the sprite with the specified color filter. Define all colors at maximum
     * (1.0f) for neutral filtering, and all at minimum (0.0f) for an all-black sprite.
     * @param colorIn Color filter
     */
    public void setColorFilter(PointColor colorIn) {
        filterColorBottomLeft = colorIn;
        filterColorBottomRight = colorIn;
        filterColorTopLeft = colorIn;
        filterColorTopRight =  colorIn;

        reDrawTextBox();
    }
    
    private float getHeight() {
        return textCharacterSize.y;
    }
    
    private float getWidth() {
        float stringLengthInScreenCoords = 0;
        
        for (int i = 0, n = currentText.length(); i < n; i++) {
            char c = currentText.charAt(i);
            stringLengthInScreenCoords += getSingleCharacterWidth(c);
        }
        return stringLengthInScreenCoords;
    }
    
    private float getSingleCharacterWidth(char character) {
        // Dynamic character sizes, for better kerning
        return textCharacterSize.x * textAtlasProperties.CHARACTER_WIDTHS[character -
                textAtlasProperties.CHARACTER_START_INDEX];
    }
    
    public float[] getFloatBufferData() {
        return characterPool.getFloatBufferData();
    }

    public void drawWithShader(TextureShaderProgram textureProgram) {
        if (!visible) return;
        characterPool.drawWithShader(textureProgram);
    }
    
    private void updateInternalPosition() {
        textBoundingBox = new RotatableBox();
        textBoundingBox.setSize(new Coordinate(getWidth(), getHeight()));
        textBoundingBox.setRotation(currentRotation);
        textBoundingBox.setPosition(userPosition, currentAlignment);
    }
    
    /**
     * Create individual sprites for each character, and place them in the drawables pool
     */
    private void reDrawTextBox() {
        characterPool.clearDrawablesPool();
        
        Coordinate lastCharacterTopRightLocation = textBoundingBox.TopLeft;
        for (int i = 0, n = currentText.length(); i < n; i++) {
            char c = currentText.charAt(i);

            Sprite singleCharSprite = new Sprite(0, 0, 1, 1,
                    textAtlasProperties.COLUMN_COUNT, textAtlasProperties.ROW_COUNT,
                    textAtlasProperties.HALF_PIXEL_SIZE);
            singleCharSprite.setSpriteSize(textCharacterSize);
            singleCharSprite.setSpriteRotationInRadians(currentRotation);
            singleCharSprite.setCurrentFrame(c - textAtlasProperties.CHARACTER_START_INDEX);
            singleCharSprite.setPosition(lastCharacterTopRightLocation, RotatableBox.Alignment.TOP_LEFT);
            singleCharSprite.setColorFilter(filterColorTopLeft, filterColorTopRight, filterColorBottomLeft, filterColorBottomRight);
            characterPool.addDrawable(singleCharSprite);
            
            // A separate box is used for the actual positions, because if the sprite
            // box is set to a size other than textCharacterSize the kerning is broken.
            // Consider that the size of the sprite is given by it's size, whereas kerning
            // seeks to overlap parts (the extra white space in the frame) of each character
            RotatableBox kerningBox = new RotatableBox();
            kerningBox.setSize(new Coordinate(getSingleCharacterWidth(c), getHeight()));
            kerningBox.setRotation(currentRotation);
            kerningBox.setPosition(lastCharacterTopRightLocation, RotatableBox.Alignment.TOP_LEFT);
            lastCharacterTopRightLocation = kerningBox.TopRight;
        }

        characterPool.updateTotalFloatArray();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof TextBox)) return false;
        TextBox other = (TextBox) obj;
        
        return other.textAtlasProperties.equals(this.textAtlasProperties) &&
                other.visible == this.visible &&
                other.currentText != null &&
                other.currentText.equals(this.currentText) &&
                other.textCharacterSize.equals(this.textCharacterSize) &&
                other.textBoundingBox.equals(this.textBoundingBox) &&
                other.currentAlignment == this.currentAlignment &&
                other.userPosition.equals(this.userPosition) &&
                other.currentRotation == this.currentRotation;
    }
}
