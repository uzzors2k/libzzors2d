package com.uzzors2k.libzzors2d.text;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;
import com.uzzors2k.libzzors2d.utils.resources.TextureHelper;

/**
 * Created by uzzors2k on 25.05.2017.
 * Based largely on work from here:
 * http://fractiousg.blogspot.no/2012/04/rendering-text-in-opengl-on-android.html
 */
public class TextTextureCreator {
    //--Constants--//
    public final static int CHAR_START = 32;           // First Character (ASCII Code)
    public final static int CHAR_END = 126;            // Last Character (ASCII Code)
    private final static int CHAR_CNT = (((CHAR_END - CHAR_START) + 1) + 1);
    private final static int CHAR_NONE = 32;            // Character to Use for Unknown (ASCII Code)
    private final static int FONT_SIZE_MIN = 6;         // Minimum Font Size (Pixels)
    private final static int FONT_SIZE_MAX = 180;       // Maximum Font Size (Pixels)
    
    private final int textureId;
    private final float[] charWidths;                   // Width of Each Character (Actual; Pixels)
    private final int rowCnt;
    private final int colCnt;
    private final Coordinate cellSize;
    private final Coordinate halfPixelSize;

    /**
     * The TextTextureCreator will create a new bitmap resource which can be used as a bitmap
     * atlas when drawing text using a sprite class.
     *
     * @param assets   - AssetManager used to access the font
     * @param file     - Filename of the font (.ttf, .otf) to use. In 'Assets' folder.
     * @param size     - Requested pixel size of font (height)
     * @param fontPadX - Extra padding per character (X+Y Axis); to prevent overlapping characters.
     * @param fontPadY - See padX
     * @throws IndexOutOfBoundsException if the calculated font size is outside of the allowed range
     */
    public TextTextureCreator(AssetManager assets, String file, int size, int fontPadX, int
            fontPadY) throws IndexOutOfBoundsException {

        // load the font and setup paint instance for drawing
        Typeface tf = Typeface.createFromAsset(assets, file);  // Create the Typeface from Font File
        Paint paint = new Paint();                      // Create Android Paint Instance
        paint.setAntiAlias(true);                       // Enable Anti Alias
        paint.setTextSize(size);                        // Set Text Size
        paint.setColor(Color.WHITE);                    // White so any color can be filtered in
        paint.setTypeface(tf);                          // Set Typeface
        
        // get font metrics
        Paint.FontMetrics fm = paint.getFontMetrics();  // Get Font Metrics
        // Calculate Font Height
        float fontHeight = (float) Math.ceil(Math.abs(fm.bottom) + Math.abs(fm.top));
        // Save Font Descent
        float fontDescent = (float) Math.ceil(Math.abs(fm.descent));
        
        // determine the width of each character (including unknown character)
        // also determine the maximum character width
        char[] s = new char[2];
        float charWidthMax = 0;
        float charHeight;
        // Maximums
        float[] w = new float[2];                       // Working Width Value
        int cnt = 0;                                    // Array Counter
        charWidths = new float[CHAR_CNT];               // Create the Array of Character Widths
        for (char c = CHAR_START; c <= CHAR_END; c++) {  // FOR Each Character
            s[0] = c;                                    // Set Character
            paint.getTextWidths(s, 0, 1, w);           // Get Character Bounds
            charWidths[cnt] = w[0];                      // Get Width
            if (charWidths[cnt] > charWidthMax) {        // IF Width Larger Than Max Width
                charWidthMax = charWidths[cnt];           // Save New Max Width
            }
            cnt++;                                       // Advance Array Counter
        }
        s[0] = CHAR_NONE;                               // Set Unknown Character
        paint.getTextWidths(s, 0, 1, w);              // Get Character Bounds
        charWidths[cnt] = w[0];                         // Get Width
        if (charWidths[cnt] > charWidthMax) {           // IF Width Larger Than Max Width
            charWidthMax = charWidths[cnt];              // Save New Max Width
        }
        
        // Normalize all character widths against the maximum
        for (char c = 0; c < charWidths.length; c++) {  // FOR Each Character
            if ((' ' - CHAR_START) == c) {
                // Kerning on ' ' space character needs a fix
                charWidths[c] = 2 * charWidths[c] / charWidthMax;
            } else {
                charWidths[c] = charWidths[c] / charWidthMax;
            }
        }
        
        // set character height to font height
        charHeight = fontHeight;                        // Set Character Height
        
        // find the maximum size, validate, and setup cell sizes
        int cellWidth = (int) charWidthMax + (2 * fontPadX);  // Set Cell Width
        int cellHeight = (int) charHeight + (2 * fontPadY);  // Set Cell Height
        int maxSize = Math.max(cellWidth, cellHeight);  // Save Max Size (Width/Height)
        if (maxSize < FONT_SIZE_MIN || maxSize > FONT_SIZE_MAX) {
            // IF Maximum Size Outside valid bounds
            throw new IndexOutOfBoundsException("Font size and/or padding outside allowed range!");
        }
        
        // set texture size based on max font size (width or height)
        // NOTE: these values are fixed, based on the defined characters. when
        // changing start/end characters (CHAR_START/CHAR_END) this will need adjustment too!
        int textureSize;
        if (maxSize <= 24)                            // IF Max Size is 18 or Less
            textureSize = 256;                           // Set 256 Texture Size
        else if (maxSize <= 40)                       // ELSE IF Max Size is 40 or Less
            textureSize = 512;                           // Set 512 Texture Size
        else if (maxSize <= 80)                       // ELSE IF Max Size is 80 or Less
            textureSize = 1024;                          // Set 1024 Texture Size
        else                                            // ELSE IF Max Size is Larger Than 80 (and Less than FONT_SIZE_MAX)
            textureSize = 2048;                          // Set 2048 Texture Size

        halfPixelSize = TextureHelper.calculateHalfPixelSize(textureSize, textureSize);

        // calculate rows/columns
        colCnt = textureSize / cellWidth;               // Calculate Number of Columns
        rowCnt = textureSize / cellHeight;            // Calculate Number of Rows
        
        // Get actual cell size, ignoring the predetermined bitmap size
        cellSize = new Coordinate(
                (float) cellWidth / (float) textureSize,
                (float) cellHeight / (float) textureSize);
        
        // create an empty bitmap
        Bitmap bitmap = Bitmap.createBitmap(textureSize, textureSize, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);           // Create Canvas for Rendering to Bitmap
        bitmap.eraseColor(0x00000000);                // Set Transparent Background (ARGB)
        
        // render each of the characters to the canvas (ie. build the font map)
        float x = fontPadX;                             // Set Start Position (X)
        float y = (cellHeight - 1) - fontDescent - fontPadY;  // Set Start Position (Y)
        for (char c = CHAR_START; c <= CHAR_END; c++) {  // FOR Each Character
            s[0] = c;                                    // Set Character to Draw
            canvas.drawText(s, 0, 1, x, y, paint);     // Draw Character
            x += cellWidth;                              // Move to Next Character
            if ((x + cellWidth - fontPadX) > textureSize) {  // IF End of Line Reached
                x = fontPadX;                             // Set X for New Row
                y += cellHeight;                          // Move Down a Row
            }
        }
        s[0] = CHAR_NONE;                               // Set Character to Use for NONE
        canvas.drawText(s, 0, 1, x, y, paint);        // Draw Character

        textureId = TextureHelper.loadTexture(bitmap);
    }

    public TextAtlasProperties getAtlasProperties() {
        return new TextAtlasProperties(
                colCnt,
                rowCnt,
                charWidths,
                CHAR_START,
                CHAR_END,
                cellSize,
                halfPixelSize);
    }
    
    /**
     * @return texture ID of the generated bitmap atlas. -1 if not generated
     */
    public int getTextTextureId() {
        return textureId;
    }
    
    /**
     * @return number of rows in the bitmap atlas
     */
    public int getRowCount() {
        return rowCnt;
    }
    
    /**
     * @return number of columns in the bitmap atlas
     */
    public int getColumnCount() {
        return colCnt;
    }
    
    /**
     * @return array containing actual widths of the characters. Can be used to improve kerning.
     */
    public float[] getCharacterWidthsArray() {
        return charWidths;
    }
    
    /**
     * @return common height/width for all cells in the bitmap atlas
     */
    public Coordinate getCellSize() {
        return cellSize;
    }
}
