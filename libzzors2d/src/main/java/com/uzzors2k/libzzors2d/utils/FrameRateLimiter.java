package com.uzzors2k.libzzors2d.utils;

import android.os.SystemClock;

/**
 * Simple helper class for limiting frame rate when rendering
 */
public class FrameRateLimiter {
    // Frame rate limiting, and monitoring
    private long frameStartTimeMs;
    private final static int FRAME_RATE_FILTERING_COUNT = 10;
    private final long[] frameIntervals;
    private int currentFrameIntervalIndex = 0;
    
    public FrameRateLimiter() {
        frameIntervals = new long[FRAME_RATE_FILTERING_COUNT];
    }
    
    /**
     * Attempts to block the caller using a sleep, using a duration equal to the difference
     * between last calling this method, and the expected time when it should have been called
     * again, assuming the target frame rate.
     *
     * @param framesPerSecond target frames per second
     * @return actual frames per second
     */
    public long limitFrameRate(int framesPerSecond) {
        long elapsedFrameTimeMs = SystemClock.elapsedRealtime() - frameStartTimeMs;
        long expectedFrameTimeMs = 1000 / framesPerSecond;
        long timeToSleepMs = expectedFrameTimeMs - elapsedFrameTimeMs;
        if (timeToSleepMs > 0) {
            elapsedFrameTimeMs = expectedFrameTimeMs;
            SystemClock.sleep(timeToSleepMs);
        }
        frameStartTimeMs = SystemClock.elapsedRealtime();
        
        currentFrameIntervalIndex++;
        if (currentFrameIntervalIndex >= FRAME_RATE_FILTERING_COUNT) {
            currentFrameIntervalIndex = 0;
        }
        frameIntervals[currentFrameIntervalIndex] = elapsedFrameTimeMs;
        
        long averageInterval = 0;
        for (int i = 0; i < FRAME_RATE_FILTERING_COUNT; i++) {
            averageInterval += frameIntervals[i];
        }
        averageInterval /= FRAME_RATE_FILTERING_COUNT;
        
        return 1000 / averageInterval;
    }
}
