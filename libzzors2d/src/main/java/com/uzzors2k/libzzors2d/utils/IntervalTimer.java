package com.uzzors2k.libzzors2d.utils;

/**
 * Simple helper class for keeping track of an interval
 */

public class IntervalTimer {
    private long timeSinceLastTrigger;
    private final int INTERVAL_FREQUENCY;
    
    /**
     * @param updateFrequency Update frequency in Hertz
     */
    public IntervalTimer(int updateFrequency) {
        INTERVAL_FREQUENCY = updateFrequency;
    }
    
    /**
     * Returns true if a length of time equal or greater than the tick rate has passed,
     * since last calling the method.
     * @param currentTime current time
     * @return true if the required time has passed since the last update
     */
    public boolean updateIntervalTimer(long currentTime) {
        long elapsedFrameTimeMs = currentTime - timeSinceLastTrigger;
        long expectedFrameTimeMs = 1000 / INTERVAL_FREQUENCY;
        if (elapsedFrameTimeMs > expectedFrameTimeMs) {
            timeSinceLastTrigger = currentTime;
            return true;
        }
        return false;
    }
}
