package com.uzzors2k.libzzors2d.utils;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import static android.opengl.Matrix.orthoM;

/**
 * Maintains screen size and projection matrix
 */
public class ScreenProjection {
    private float screenWidth = 1.0f;
    private float screenHeight = 1.0f;
    
    private final float[] projectionMatrix = new float[16];
    
    /**
     * Update the projection matrix once the screen size has changed.
     *
     * @param width new screen width, pixels
     * @param height new screen height, pixels
     */
    public void onSurfaceChanged(int width, int height) {
        final float aspectRatio = width > height ?
                (float) width / (float) height :
                (float) height / (float) width;
    
        if (width > height) {
            // Landscape
            orthoM(projectionMatrix, 0, -aspectRatio, aspectRatio, -1f, 1f, -1f, 1f);
            // Height is locked to 1.0f from center to border, so the width will vary
            screenHeight = 1.0f;
            screenWidth = aspectRatio;
        } else {
            // Portrait or square
            orthoM(projectionMatrix, 0, -1f, 1f, -aspectRatio, aspectRatio, -1f, 1f);
            // Width is locked to 1.0f from center to border, so the width will vary
            screenWidth = 1.0f;
            screenHeight = aspectRatio;
        }
        
        screenWidth *= 2;
        screenHeight *= 2;
    }

    public Coordinate getTouchCoordinatesOnScreen(float touchX, float touchY) {
        // Normalize the touch coordinates to the actual aspect ratio
        float normalizedX = touchX * screenWidth / 2;
        float normalizedY = touchY * screenHeight / 2;
        return new Coordinate(normalizedX, normalizedY);
    }
    
    public float[] getProjectionMatrix() {
        return projectionMatrix;
    }
    
    /**
     * @return screen width, normalized to 2.0. Distance from one border to the other.
     */
    public float getScreenWidth() {
        return screenWidth;
    }
    
    /**
     * @return screen height, normalized to 2.0. Distance from one border to the other.
     */
    public float getScreenHeight() {
        return screenHeight;
    }
}
