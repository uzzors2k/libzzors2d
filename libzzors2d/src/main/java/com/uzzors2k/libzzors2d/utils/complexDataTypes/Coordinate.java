package com.uzzors2k.libzzors2d.utils.complexDataTypes;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Simple coordinate/vector class
 */
public class Coordinate {
    public float x;
    public float y;
    
    public Coordinate(float xCoordinate, float yCoordinate) {
        x = xCoordinate;
        y = yCoordinate;
    }
    
    public Coordinate(Coordinate otherCoord) {
        x = otherCoord.x;
        y = otherCoord.y;
    }
    
    /**
     * Adds another coordinate to this
     *
     * @param otherCoord coordinate to add
     * @return this object
     */
    public Coordinate add(Coordinate otherCoord) {
        x += otherCoord.x;
        y += otherCoord.y;
        
        return this;
    }
    
    /**
     * Subtract another coordinate from this
     *
     * @param otherCoord coordinate to subtract
     * @return this object
     */
    public Coordinate subtract(Coordinate otherCoord) {
        x -= otherCoord.x;
        y -= otherCoord.y;
        
        return this;
    }
    
    /**
     * Rotate this coordinate around the origin
     *
     * @param radians angle to rotate
     * @return this object
     */
    public Coordinate rotateAroundOrigin(double radians) {
        float oldX = x;
        float oldY = y;
        x = (float) (oldX * cos(radians) - oldY * sin(radians));
        y = (float) (oldX * sin(radians) + oldY * cos(radians));
        
        return this;
    }
    
    /**
     * Rotate this coordinate around another coordinate
     *
     * @param radians        angle to rotate
     * @param rotationCenter rotation center
     * @return this object
     */
    public Coordinate rotateAroundCoordinate(double radians, Coordinate rotationCenter) {
        this.subtract(rotationCenter);
        this.rotateAroundOrigin(radians);
        this.add(rotationCenter);
        
        return this;
    }
    
    /**
     * Scale this vector by the factor given
     *
     * @param constant factor to multiply x and y with
     * @return this object
     */
    public Coordinate multiply(float constant) {
        x *= constant;
        y *= constant;
        
        return this;
    }

    /**
     * Scale this vector by the factor given
     *
     * @param constant factor to multiply x and y with
     * @return this object
     */
    public Coordinate divide(float constant) {
        x /= constant;
        y /= constant;

        return this;
    }
    
    /**
     * Calculate sqrt(x^2 + y^2)
     *
     * @return magnitude or length of the vector
     */
    public float getMagnitude() {
        return (float)Math.sqrt(x*x + y*y);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof Coordinate)) return false;
        Coordinate other = (Coordinate) obj;
        
        return other.x == this.x &&
                other.y == this.y;
    }

    public Coordinate deepCopy() {
        return new Coordinate(x, y);
    }
}
