package com.uzzors2k.libzzors2d.utils.complexDataTypes;

/**
 * Box object which can be moved, rotated, mirrored. Eirik Taylor, 01.10.2017
 */

public class RotatableBox {

    public enum Alignment {
        TOP_LEFT,
        TOP_RIGHT,
        TOP_CENTER,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        BOTTOM_CENTER,
        CENTER,
        LEFT_CENTER,
        RIGHT_CENTER
    }
    
    public Coordinate TopLeft;
    public Coordinate TopRight;
    public Coordinate BottomLeft;
    public Coordinate BottomRight;
    
    private Coordinate centerLocation;
    private Coordinate size;
    private double rotation;
    private boolean boolMirroredHorizontally;
    private boolean boolMirroredVertically;
    private Alignment currentAlignment;
    private Coordinate userLocation;
    
    /**
     * Simple box, which can be manipulated in a number of ways
     */
    public RotatableBox() {
        centerLocation = new Coordinate(0.0f, 0.0f);
        size = new Coordinate(0.0f, 0.0f);
        rotation = 0.0f;
        boolMirroredHorizontally = false;
        boolMirroredVertically = false;
        currentAlignment = Alignment.TOP_LEFT;
        userLocation = new Coordinate(0.0f, 0.0f);
        
        updateCornerLocations();
    }
    
    /**
     * Simple box, which can be manipulated in a number of ways
     *
     * @param topLeft      top left corner location
     * @param startingSize size of the box
     */
    public RotatableBox(Coordinate topLeft, Coordinate startingSize) {
        rotation = 0.0f;
        boolMirroredHorizontally = false;
        boolMirroredVertically = false;
        currentAlignment = Alignment.TOP_LEFT;
        userLocation = topLeft;
        size = startingSize;
        
        updateCenterLocation();
        updateCornerLocations();
    }
    
    /**
     * Rotate the box with the given angle in radians
     *
     * @param rotationRadians angle in radians to rotate
     */
    public void setRotation(double rotationRadians) {
        rotation = rotationRadians;
        updateCornerLocations();
    }
    
    /**
     * Mirror/flip the box coordinates around a given axis
     *
     * @param horizontal mirror horizontally
     * @param vertical   mirror vertically
     */
    public void setMirroring(boolean horizontal, boolean vertical) {
        boolMirroredHorizontally = horizontal;
        boolMirroredVertically = vertical;
        updateCornerLocations();
    }
    
    /**
     * Sets the size of the rotatable box. NOTE: This must be set prior to using
     * setPosition.
     *
     * @param newSize - new size of the box
     */
    public void setSize(Coordinate newSize) {
        size = newSize;
        updateCenterLocation();
        updateCornerLocations();
    }
    
    public float getHeight() {
        return size.y;
    }
    
    public float getWidth() {
        return size.x;
    }
    
    /**
     * Recalculate the location of all four corners
     */
    private void updateCornerLocations() {
        // Calculate the corner locations based on the current internal state
        
        // First determine the location based on size alone, with no rotation
        TopLeft = new Coordinate(-size.x / 2, size.y / 2);
        TopRight = new Coordinate(size.x / 2, size.y / 2);
        BottomLeft = new Coordinate(-size.x / 2, -size.y / 2);
        BottomRight = new Coordinate(size.x / 2, -size.y / 2);
        
        // Move center
        TopLeft.add(centerLocation);
        TopRight.add(centerLocation);
        BottomLeft.add(centerLocation);
        BottomRight.add(centerLocation);
        
        // Then apply mirroring effects, by swapping coordinates
        mirrorCoordinates();
        
        // Finally rotate around the center coordinate
        TopLeft.rotateAroundCoordinate(rotation, centerLocation);
        TopRight.rotateAroundCoordinate(rotation, centerLocation);
        BottomLeft.rotateAroundCoordinate(rotation, centerLocation);
        BottomRight.rotateAroundCoordinate(rotation, centerLocation);
    }
    
    /**
     * Swap coordinates based on requested mirroring
     */
    private void mirrorCoordinates() {
        // An alternative would be negating the size. E.g. Horizontal would be -1.0f, 1.0f
        // and vertical would be 1.0f, -1.0f
        
        if (boolMirroredHorizontally) {
            Coordinate temp = new Coordinate(TopLeft);
            TopLeft = new Coordinate(TopRight);
            TopRight = new Coordinate(temp);
            
            temp = new Coordinate(BottomLeft);
            BottomLeft = new Coordinate(BottomRight);
            BottomRight = new Coordinate(temp);
        }
        
        if (boolMirroredVertically) {
            Coordinate temp = new Coordinate(TopLeft);
            TopLeft = new Coordinate(BottomLeft);
            BottomLeft = new Coordinate(temp);
            
            temp = new Coordinate(TopRight);
            TopRight = new Coordinate(BottomRight);
            BottomRight = new Coordinate(temp);
        }
    }
    
    /**
     * Set the position, in relation to the alignment given
     *
     * @param position  coordinate which will be set as the given alignment of the box
     * @param alignment which part of the box the position will be
     */
    public void setPosition(Coordinate position, Alignment alignment) {
        currentAlignment = alignment;
        userLocation = position;
        
        updateCenterLocation();
        updateCornerLocations();
    }
    
    /**
     * Call this any time the size, or userlocation/alignment is changed
     */
    private void updateCenterLocation() {
        // Use the selected alignment, and current rotation to move the center position
        // Calling updateCornerLocations() will then automatically calculate all other positions

        if (currentAlignment == Alignment.CENTER)
        {
            centerLocation = userLocation.deepCopy();
            return;
        }

        Coordinate newCenter;
        switch (currentAlignment) {
            case TOP_LEFT: {
                newCenter = new Coordinate(userLocation.x + size.x / 2, userLocation.y -
                        size.y / 2);
                break;
            }
            
            case TOP_RIGHT: {
                newCenter = new Coordinate(userLocation.x - size.x / 2, userLocation.y -
                        size.y / 2);
                break;
            }
            
            case TOP_CENTER: {
                newCenter = new Coordinate(userLocation.x, userLocation.y - size.y / 2);
                break;
            }
            
            case BOTTOM_LEFT: {
                newCenter = new Coordinate(userLocation.x + size.x / 2, userLocation.y +
                        size.y / 2);
                break;
            }
            
            case BOTTOM_RIGHT: {
                newCenter = new Coordinate(userLocation.x - size.x / 2, userLocation.y +
                        size.y / 2);
                break;
            }
            
            case BOTTOM_CENTER: {
                newCenter = new Coordinate(userLocation.x, userLocation.y + size.y / 2);
                break;
            }

            case LEFT_CENTER: {
                newCenter = new Coordinate(userLocation.x + size.x / 2, userLocation.y);
                break;
            }

            case RIGHT_CENTER: {
                newCenter = new Coordinate(userLocation.x - size.x / 2, userLocation.y);
                break;
            }

            default:
                throw new IllegalStateException("Unexpected value: " + currentAlignment);
        }

        newCenter.rotateAroundCoordinate(rotation, userLocation);
        centerLocation = newCenter;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof RotatableBox)) return false;
        RotatableBox other = (RotatableBox) obj;
        
        return other.TopLeft.equals(this.TopLeft) &&
                other.TopRight.equals(this.TopRight) &&
                other.BottomLeft.equals(this.BottomLeft) &&
                other.BottomRight.equals(this.BottomRight) &&
                other.centerLocation.equals(this.centerLocation) &&
                other.size.equals(this.size) &&
                other.rotation == this.rotation &&
                other.boolMirroredHorizontally == this.boolMirroredHorizontally &&
                other.boolMirroredVertically == this.boolMirroredVertically &&
                other.currentAlignment == this.currentAlignment &&
                other.userLocation.equals(this.userLocation);
    }

    public RotatableBox deepCopy() {
        RotatableBox copy = new RotatableBox();
        copy.centerLocation = centerLocation.deepCopy();
        copy.size = size.deepCopy();
        copy.rotation = rotation;
        copy.boolMirroredHorizontally = boolMirroredHorizontally;
        copy.boolMirroredVertically = boolMirroredVertically;
        copy.currentAlignment = currentAlignment;
        copy.userLocation = userLocation.deepCopy();
        copy.updateCornerLocations();
        return copy;
    }
}
