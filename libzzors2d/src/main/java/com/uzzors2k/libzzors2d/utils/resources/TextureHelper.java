package com.uzzors2k.libzzors2d.utils.resources;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import static android.opengl.GLES20.GL_NEAREST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glTexParameteri;

public class TextureHelper {
    
    public static int loadTexture(final Context context, final int resourceId) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false; // No pre-scaling
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);
        
        return loadTexture(bitmap);
    }
    
    /**
     * Loads a bitmap from resources and returns a resource ID
     *
     * @param bitmap bitmap from resources
     * @return resource ID associated with the texture
     * @throws RuntimeException if the texture could not be loaded
     */
    public static int loadTexture(Bitmap bitmap) {
        final int[] textureHandle = new int[1];
        
        glGenTextures(1, textureHandle, 0);
        
        if (textureHandle[0] != 0) {
            // Bind to the texture in OpenGL
            glBindTexture(GL_TEXTURE_2D, textureHandle[0]);

            // Set filtering of textures so that they remain pixelated
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
            
            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle();
        }
        
        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }
        
        return textureHandle[0];
    }

    public static Coordinate calculateHalfPixelSize(int columns, int rows, int cellSizeX, int cellSizeY) {
        return new Coordinate(
                (float)(0.5 * ((1.0f / columns) / cellSizeX)),
                (float)(0.5 * ((1.0f / rows) / cellSizeY)));
    }

    public static Coordinate calculateHalfPixelSize(int textureWidthInPixels, int textureHeightInPixels) {
        return new Coordinate(
                (float)(0.5 * (1.0f / textureWidthInPixels)),
                (float)(0.5 * (1.0f / textureHeightInPixels)));
    }
}

