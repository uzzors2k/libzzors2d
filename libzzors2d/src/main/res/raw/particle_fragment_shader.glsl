precision mediump float;
varying vec3 v_Color;
varying vec3 v_DecayFactor;
varying float v_ElapsedTime;

void main()
{
    float xDistance = 0.5 - gl_PointCoord.x;
    float yDistance = 0.5 - gl_PointCoord.y;
    float distanceFromCenter = sqrt(xDistance * xDistance + yDistance * yDistance);

    if (distanceFromCenter > 0.5) {
        // Make the points circular
        discard;
    } else {
        gl_FragColor = vec4(v_Color, (v_DecayFactor / v_ElapsedTime + 0.01));
    }
}
