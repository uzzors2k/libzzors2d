uniform mat4 u_Matrix;
uniform float u_Time;

attribute vec3 a_Position;
attribute vec3 a_Color;
attribute vec3 a_DirectionVector;
attribute vec3 a_GravityVector;
attribute vec3 a_DecayFactor;
attribute float a_ParticleStartTime;

varying vec3 v_Color;
varying vec3 v_DecayFactor;
varying float v_ElapsedTime;

void main()
{
    v_Color = a_Color;
    v_DecayFactor = a_DecayFactor;
    v_ElapsedTime = u_Time - a_ParticleStartTime;

    vec3 gravityFactor = v_ElapsedTime * v_ElapsedTime * a_GravityVector;
    vec3 currentPosition = a_Position + (a_DirectionVector * v_ElapsedTime);
    currentPosition += gravityFactor;

    gl_Position = u_Matrix * vec4(currentPosition, 1.0);
    gl_PointSize = 10.0;
}
