precision mediump float;

uniform sampler2D u_TextureUnit;
uniform int u_ColoringMode;
// Color replacement
uniform vec4 u_RedReplacement;
uniform vec4 u_GreenReplacement;
uniform vec4 u_BlueReplacement;
// REFACTOR: These booleans are defined in u_ColoringMode as a bit mask. Could simplify.
uniform bool u_ReplaceRed;
uniform bool u_ReplaceGreen;
uniform bool u_ReplaceBlue;

varying vec2 v_TextureCoordinates;
varying vec4 v_Color;

void main()
{
    vec4 textureColor = texture2D(u_TextureUnit, v_TextureCoordinates);

    if ((u_ColoringMode == 0) || (u_ColoringMode == 1))
    {
        gl_FragColor = textureColor * v_Color;
        return;
    }
    else if (u_ColoringMode >= 2)
    {
        float zeroLimit = 0.0001;

        // Replace R, G, B shades with the specified color
        if (u_ReplaceRed &&
            (textureColor.r > zeroLimit) &&
            ((textureColor.g < zeroLimit) && (textureColor.b < zeroLimit)))
        {
            // Red shades, convert to white before coloring
            textureColor.g = textureColor.r;
            textureColor.b = textureColor.r;
            gl_FragColor = textureColor * u_RedReplacement;
            return;
        }
        else if (u_ReplaceGreen &&
            (textureColor.g > zeroLimit) &&
            ((textureColor.r < zeroLimit) && (textureColor.b < zeroLimit)))
        {
            // Green shades, convert to white before coloring
            textureColor.r = textureColor.g;
            textureColor.b = textureColor.g;
            gl_FragColor = textureColor * u_GreenReplacement;
            return;
        }
        else if (u_ReplaceBlue &&
            (textureColor.b > zeroLimit) &&
            ((textureColor.r < zeroLimit) && (textureColor.g < zeroLimit)))
        {
            // Blue shades, convert to white before coloring
            textureColor.r = textureColor.b;
            textureColor.g = textureColor.b;
            gl_FragColor = textureColor * u_BlueReplacement;
            return;
        }

        gl_FragColor = textureColor * v_Color;
        return;
    }

    // Error, undefined coloration mode. Just make the sprite black.
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
}
