uniform mat4 u_Matrix;

attribute vec4 a_Position;  
attribute vec2 a_TextureCoordinates;
attribute vec4 a_TextureColor;

varying vec2 v_TextureCoordinates;
varying vec4 v_Color;

void main()                    
{                            
    v_TextureCoordinates = a_TextureCoordinates;	  	  
    gl_Position = u_Matrix * a_Position;
    v_Color = a_TextureColor;
}
