package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for coordinate class, 01.10.2017
 */

public class CoordinateUnitTests {
    private final float DELTA = (float) 0.0001;
    
    @Test
    public void FullRotation() {
        // Arrange
        Coordinate testCoordinate = new Coordinate(1.0f, 1.0f);
        double threeSixtyRotationInRadians = 2 * Math.PI;
        
        // Act
        testCoordinate.rotateAroundOrigin(threeSixtyRotationInRadians);
        
        // Assert
        assertEquals(1.0f, testCoordinate.x, DELTA);
        assertEquals(1.0f, testCoordinate.y, DELTA);
    }
    
    @Test
    public void NinetyDegreeRotation() {
        // Arrange
        Coordinate testCoordinate = new Coordinate(0.0f, 1.0f);
        double ninetyDegreesInRadians = Math.PI / 2;
        
        // Act
        testCoordinate.rotateAroundOrigin(ninetyDegreesInRadians);
        
        // Assert
        assertEquals(-1.0f, testCoordinate.x, DELTA);
        assertEquals(0.0f, testCoordinate.y, DELTA);
    }
    
    @Test
    public void MinusNinetyDegreeRotation() {
        // Arrange
        Coordinate testCoordinate = new Coordinate(1.0f, 0.0f);
        double minusNinetyDegreesInRadians = -Math.PI / 2;
        
        // Act
        testCoordinate.rotateAroundOrigin(minusNinetyDegreesInRadians);
        
        // Assert
        assertEquals(0.0f, testCoordinate.x, DELTA);
        assertEquals(-1.0f, testCoordinate.y, DELTA);
    }
    
    @Test
    public void RotationAroundAPoint() {
        // Arrange
        Coordinate testCoordinate = new Coordinate(2.0f, 0.0f);
        Coordinate rotationCenter = new Coordinate(1.0f, 0.0f);
        double oneEightyDegreesInRadians = Math.PI;
        
        // Act
        testCoordinate.rotateAroundCoordinate(oneEightyDegreesInRadians, rotationCenter);
        
        // Assert
        assertEquals(0.0f, testCoordinate.x, DELTA);
        assertEquals(0.0f, testCoordinate.y, DELTA);
    }
    
    @Test
    public void Coordinate_Equality_test() {
        Coordinate identicalCoordinate1 = new Coordinate(9, 7);
        Coordinate identicalCoordinate2 = new Coordinate(9, 7);
        
        boolean equal = identicalCoordinate1.equals(identicalCoordinate2);
        assertTrue(equal);
    }
    
    @Test
    public void Coordinate_InEquality_test() {
        Coordinate identicalCoordinate1 = new Coordinate(7, 7);
        Coordinate identicalCoordinate2 = new Coordinate(2, 5);
        
        boolean equal = identicalCoordinate1.equals(identicalCoordinate2);
        assertFalse(equal);
    }

    @Test
    public void Coordinate_DeepCopy_test() {
        Coordinate identicalCoordinate1 = new Coordinate(7, 7);
        Coordinate copy = identicalCoordinate1.deepCopy();

        assertEquals(identicalCoordinate1, copy);

        copy.x = 3;
        copy.y = 1;

        boolean xEqual = (identicalCoordinate1.x == copy.x);
        boolean yEqual = (identicalCoordinate1.y == copy.y);

        assertFalse(xEqual);
        assertFalse(yEqual);
    }
}
