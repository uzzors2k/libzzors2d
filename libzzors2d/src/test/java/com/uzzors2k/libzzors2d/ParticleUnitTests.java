package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.particles.ParticleGenerator;
import com.uzzors2k.libzzors2d.particles.ParticleGeneratorPool;
import com.uzzors2k.libzzors2d.shapes.Point;
import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for particle system
 */

public class ParticleUnitTests {
    private final float DELTA = (float) 0.0001;
    private final int MAX_PARTICLES = 512;
    
    @Test
    public void Particles_can_be_added() {
        // Arrange
        PointColor redColor = new PointColor(1.0f, 0, 0, 1.0f);
        Point spawnPositionA = new Point(new Coordinate(-0.5f, -0.5f), redColor);
        Coordinate startingDirection = new Coordinate(0.5f, 0.2f);
        Coordinate gravityDirection = new Coordinate(0.0f, -1.0f);
        float decayFactor = 1.2f;
        // No random distribution desired when testing
        float speedVariance = 0.0f;
        float angleVariance = 0.0f;
        float particleStartTime = 1000;
        int particlesToAdd = 2;
        
        // Act
        ParticleGenerator testParticleShooter = new ParticleGenerator(MAX_PARTICLES, spawnPositionA,
                spawnPositionA, startingDirection, gravityDirection, speedVariance,
                angleVariance, decayFactor);
        
        testParticleShooter.addParticles(particlesToAdd, particleStartTime);
        
        // Assert
        assertEquals(particlesToAdd, testParticleShooter.getVertexSize());
        
        float[] bufferData = testParticleShooter.getFloatBufferData();
        int currentIndex = 0;
        
        // Position
        assertEquals(spawnPositionA.location.x, bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.location.y, bufferData[currentIndex++], DELTA);
        // Color
        assertEquals(spawnPositionA.color.getRedComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getGreenComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getBlueComponent(), bufferData[currentIndex++], DELTA);
        // Direction
        assertEquals(startingDirection.x, bufferData[currentIndex++], DELTA);
        assertEquals(startingDirection.y, bufferData[currentIndex++], DELTA);
        // Gravity
        assertEquals(gravityDirection.x, bufferData[currentIndex++], DELTA);
        assertEquals(gravityDirection.y, bufferData[currentIndex++], DELTA);
        // Decay factor
        assertEquals(decayFactor, bufferData[currentIndex++], DELTA);
        // Start time
        assertEquals(particleStartTime, bufferData[currentIndex++], DELTA);
        
        // Position
        assertEquals(spawnPositionA.location.x, bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.location.y, bufferData[currentIndex++], DELTA);
        // Color
        assertEquals(spawnPositionA.color.getRedComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getGreenComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getBlueComponent(), bufferData[currentIndex++], DELTA);
        // Direction
        assertEquals(startingDirection.x, bufferData[currentIndex++], DELTA);
        assertEquals(startingDirection.y, bufferData[currentIndex++], DELTA);
        // Gravity
        assertEquals(gravityDirection.x, bufferData[currentIndex++], DELTA);
        assertEquals(gravityDirection.y, bufferData[currentIndex++], DELTA);
        // Decay factor
        assertEquals(decayFactor, bufferData[currentIndex++], DELTA);
        // Start time
        assertEquals(particleStartTime, bufferData[currentIndex], DELTA);
    }
    
    @Test
    public void Particles_can_be_added_to_pool() {
        // Arrange
        ParticleGeneratorPool testParticlePool = new ParticleGeneratorPool();
        
        PointColor greenColor = new PointColor(0.0f, 1.0f, 0.0f, 1.0f);
        PointColor redColor = new PointColor(1.0f, 0.0f, 0.0f, 1.0f);
        Point spawnPositionA = new Point(new Coordinate(0.5f, 0.5f), greenColor);
        Point spawnPositionB = new Point(new Coordinate(0.2f, 0.9f), redColor);
        Coordinate startingDirectionA = new Coordinate(-0.9f, 1.2f);
        Coordinate startingDirectionB = new Coordinate(1.9f, -0.6f);
        Coordinate gravityDirectionA = new Coordinate(0.0f, -9.0f);
        Coordinate gravityDirectionB = new Coordinate(1.0f, 0.0f);
        float decayFactorA = 0.3f;
        float decayFactorB = 3.3f;
        // No random distribution desired when testing
        float speedVariance = 0.0f;
        float angleVariance = 0.0f;
        float particleStartTimeA = 3465434;
        float particleStartTimeB = 6576;
        
        // Act
        ParticleGenerator testParticleShooterA = new ParticleGenerator(MAX_PARTICLES, spawnPositionA,
                spawnPositionA, startingDirectionA, gravityDirectionA, speedVariance,
                angleVariance, decayFactorA);
        ParticleGenerator testParticleShooterB = new ParticleGenerator(MAX_PARTICLES, spawnPositionB,
                spawnPositionB, startingDirectionB, gravityDirectionB, speedVariance,
                angleVariance, decayFactorB);
        
        testParticlePool.addDrawable(testParticleShooterA);
        testParticlePool.addDrawable(testParticleShooterB);
        
        testParticleShooterA.addParticles(1, particleStartTimeA);
        testParticleShooterB.addParticles(1, particleStartTimeB);
        testParticlePool.updateTotalFloatArray();
        
        // Assert
        float[] bufferData = testParticlePool.getFloatBufferData();
        int currentIndex = 0;
        
        ///////// Particles A
        // Position
        assertEquals(spawnPositionA.location.x, bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.location.y, bufferData[currentIndex++], DELTA);
        // Color
        assertEquals(spawnPositionA.color.getRedComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getGreenComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getBlueComponent(), bufferData[currentIndex++], DELTA);
        // Direction
        assertEquals(startingDirectionA.x, bufferData[currentIndex++], DELTA);
        assertEquals(startingDirectionA.y, bufferData[currentIndex++], DELTA);
        // Gravity
        assertEquals(gravityDirectionA.x, bufferData[currentIndex++], DELTA);
        assertEquals(gravityDirectionA.y, bufferData[currentIndex++], DELTA);
        // Decay factor
        assertEquals(decayFactorA, bufferData[currentIndex++], DELTA);
        // Start time
        assertEquals(particleStartTimeA, bufferData[currentIndex++], DELTA);
        
        ///////// Particles B
        // Position
        assertEquals(spawnPositionB.location.x, bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionB.location.y, bufferData[currentIndex++], DELTA);
        // Color
        assertEquals(spawnPositionB.color.getRedComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionB.color.getGreenComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionB.color.getBlueComponent(), bufferData[currentIndex++], DELTA);
        // Direction
        assertEquals(startingDirectionB.x, bufferData[currentIndex++], DELTA);
        assertEquals(startingDirectionB.y, bufferData[currentIndex++], DELTA);
        // Gravity
        assertEquals(gravityDirectionB.x, bufferData[currentIndex++], DELTA);
        assertEquals(gravityDirectionB.y, bufferData[currentIndex++], DELTA);
        // Decay factor
        assertEquals(decayFactorB, bufferData[currentIndex++], DELTA);
        // Start time
        assertEquals(particleStartTimeB, bufferData[currentIndex], DELTA);
    }
    
    @Test
    public void Particle_equality_tests() {
        // Arrange
        PointColor redColor = new PointColor(1.0f, 0, 0, 1.0f);
        Point spawnPositionA = new Point(new Coordinate(-0.5f, -0.5f), redColor);
        Coordinate startingDirection = new Coordinate(0.5f, 0.2f);
        Coordinate gravityDirection = new Coordinate(0.0f, -1.0f);
        float decayFactor = 1.2f;
        // No random distribution desired when testing
        float speedVariance = 0.0f;
        float angleVariance = 0.0f;
        float particleStartTime = 1000;
        int particlesToAdd = 2;
        
        // Act
        ParticleGenerator indenticalParticleGenerator1 = new ParticleGenerator(MAX_PARTICLES,
                spawnPositionA, spawnPositionA, startingDirection, gravityDirection, speedVariance,
                angleVariance, decayFactor);
        indenticalParticleGenerator1.addParticles(particlesToAdd, particleStartTime);
        
        ParticleGenerator indenticalParticleGenerator2 = new ParticleGenerator(MAX_PARTICLES,
                spawnPositionA, spawnPositionA, startingDirection, gravityDirection, speedVariance,
                angleVariance, decayFactor);
        indenticalParticleGenerator2.addParticles(particlesToAdd, particleStartTime);
        
        // Assert
        boolean equal = indenticalParticleGenerator1.equals(indenticalParticleGenerator2);
        assertTrue(equal);
    }
    
    @Test
    public void Particle_Inequality_tests() {
        // Arrange
        PointColor redColor = new PointColor(1.0f, 0, 0, 1.0f);
        Point spawnPositionA = new Point(new Coordinate(-0.5f, -0.5f), redColor);
        Coordinate startingDirection = new Coordinate(0.5f, 0.2f);
        Coordinate gravityDirection = new Coordinate(0.0f, -1.0f);
        float decayFactor = 1.2f;
        // No random distribution desired when testing
        float speedVariance = 0.0f;
        float angleVariance = 0.0f;
        float particleStartTime = 1000;
        int particlesToAdd = 2;
        
        // Act
        ParticleGenerator indenticalParticleGenerator1 = new ParticleGenerator(MAX_PARTICLES,
                spawnPositionA, spawnPositionA, startingDirection, gravityDirection, speedVariance,
                angleVariance, decayFactor);
        indenticalParticleGenerator1.addParticles(particlesToAdd, particleStartTime);
        
        ParticleGenerator indenticalParticleGenerator2 = new ParticleGenerator(MAX_PARTICLES,
                spawnPositionA, spawnPositionA, startingDirection, gravityDirection, speedVariance,
                angleVariance, decayFactor);
        indenticalParticleGenerator2.addParticles(particlesToAdd + 1, particleStartTime);
        
        // Assert
        boolean equal = indenticalParticleGenerator1.equals(indenticalParticleGenerator2);
        assertFalse(equal);
    }
    
    @Test
    public void Particle_source_can_be_removed() {
        // Arrange
        ParticleGeneratorPool testParticlePool = new ParticleGeneratorPool();
        
        PointColor greenColor = new PointColor(0.0f, 1.0f, 0.0f, 1.0f);
        PointColor redColor = new PointColor(1.0f, 0.0f, 0.0f, 1.0f);
        Point spawnPositionA = new Point(new Coordinate(0.5f, 0.5f), greenColor);
        Point spawnPositionB = new Point(new Coordinate(0.2f, 0.9f), redColor);
        Coordinate startingDirectionA = new Coordinate(-0.9f, 1.2f);
        Coordinate startingDirectionB = new Coordinate(1.9f, -0.6f);
        Coordinate gravityDirectionA = new Coordinate(0.0f, -9.0f);
        Coordinate gravityDirectionB = new Coordinate(1.0f, 0.0f);
        float decayFactorA = 0.3f;
        float decayFactorB = 3.3f;
        // No random distribution desired when testing
        float speedVariance = 0.0f;
        float angleVariance = 0.0f;
        float particleStartTimeA = 3465434;
        float particleStartTimeB = 6576;
        
        ParticleGenerator testParticleShooterA = new ParticleGenerator(MAX_PARTICLES, spawnPositionA,
                spawnPositionA, startingDirectionA, gravityDirectionA, speedVariance,
                angleVariance, decayFactorA);
        ParticleGenerator testParticleShooterB = new ParticleGenerator(MAX_PARTICLES, spawnPositionB,
                spawnPositionB, startingDirectionB, gravityDirectionB, speedVariance,
                angleVariance, decayFactorB);
        
        testParticlePool.addDrawable(testParticleShooterA);
        testParticlePool.addDrawable(testParticleShooterB);
        
        testParticleShooterA.addParticles(1, particleStartTimeA);
        testParticleShooterB.addParticles(1, particleStartTimeB);
        testParticlePool.updateTotalFloatArray();
        
        // Act
        testParticlePool.removeObject(testParticleShooterB);
        testParticlePool.updateTotalFloatArray();
        
        // Assert
        float[] bufferData = testParticlePool.getFloatBufferData();
        
        assertEquals(11, bufferData.length);
        
        int currentIndex = 0;
        ///////// Particles A
        // Position
        assertEquals(spawnPositionA.location.x, bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.location.y, bufferData[currentIndex++], DELTA);
        // Color
        assertEquals(spawnPositionA.color.getRedComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getGreenComponent(), bufferData[currentIndex++], DELTA);
        assertEquals(spawnPositionA.color.getBlueComponent(), bufferData[currentIndex++], DELTA);
        // Direction
        assertEquals(startingDirectionA.x, bufferData[currentIndex++], DELTA);
        assertEquals(startingDirectionA.y, bufferData[currentIndex++], DELTA);
        // Gravity
        assertEquals(gravityDirectionA.x, bufferData[currentIndex++], DELTA);
        assertEquals(gravityDirectionA.y, bufferData[currentIndex++], DELTA);
        // Decay factor
        assertEquals(decayFactorA, bufferData[currentIndex++], DELTA);
        // Start time
        assertEquals(particleStartTimeA, bufferData[currentIndex], DELTA);
    }
}
