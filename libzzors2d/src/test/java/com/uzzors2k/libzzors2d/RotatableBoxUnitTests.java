package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for rotatable box class. 01.10.2017
 */

public class RotatableBoxUnitTests {
    private final float DELTA = (float) 0.0001;
    
    @Test
    public void Center_is_still_Center() {
        // Arrange
        RotatableBox testBox = new RotatableBox();
        Coordinate centerLocation = new Coordinate(0.2f, -0.4f);
        Coordinate boxSize = new Coordinate(0.1f, 0.1f);
        
        // Act
        testBox.setPosition(centerLocation, RotatableBox.Alignment.CENTER);
        testBox.setSize(boxSize);
        
        // Assert
        Coordinate expectedTopLeft = new Coordinate(centerLocation.x - boxSize.x / 2,
                centerLocation.y + boxSize.y / 2);
        assertEquals(expectedTopLeft.x, testBox.TopLeft.x, DELTA);
        assertEquals(expectedTopLeft.y, testBox.TopLeft.y, DELTA);
    }
    
    @Test
    public void TopLeft_is_still_TopLeft() {
        // Arrange
        Coordinate topLeft = new Coordinate(0.2f, -0.4f);
        Coordinate boxSize = new Coordinate(0.1f, 0.1f);
        
        // Act
        RotatableBox testBox = new RotatableBox(topLeft, boxSize);
        // Assert
        assertEquals(topLeft.x, testBox.TopLeft.x, DELTA);
        assertEquals(topLeft.y, testBox.TopLeft.y, DELTA);
    }
    
    @Test
    public void Rotation_works_on_centered_box() {
        // Arrange
        Coordinate boxSize = new Coordinate(2.0f, 2.0f);
        double rotationAngle = Math.PI / 8;
        
        // Act
        RotatableBox testBox = new RotatableBox();
        testBox.setSize(boxSize);
        testBox.setRotation(rotationAngle);
        testBox.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.CENTER);
        
        // Assert
        Coordinate expectedTopRightCorner = new Coordinate(1.0f, 1.0f);
        expectedTopRightCorner.rotateAroundOrigin(rotationAngle);
        assertEquals(expectedTopRightCorner.x, testBox.TopRight.x, DELTA);
        assertEquals(expectedTopRightCorner.y, testBox.TopRight.y, DELTA);
        
        Coordinate expectedTopLeftCorner = new Coordinate(-1.0f, 1.0f);
        expectedTopLeftCorner.rotateAroundOrigin(rotationAngle);
        assertEquals(expectedTopLeftCorner.x, testBox.TopLeft.x, DELTA);
        assertEquals(expectedTopLeftCorner.y, testBox.TopLeft.y, DELTA);
        
        Coordinate expectedBottomLeftCorner = new Coordinate(-1.0f, -1.0f);
        expectedBottomLeftCorner.rotateAroundOrigin(rotationAngle);
        assertEquals(expectedBottomLeftCorner.x, testBox.BottomLeft.x, DELTA);
        assertEquals(expectedBottomLeftCorner.y, testBox.BottomLeft.y, DELTA);
        
        Coordinate expectedBottomRightCorner = new Coordinate(1.0f, -1.0f);
        expectedBottomRightCorner.rotateAroundOrigin(rotationAngle);
        assertEquals(expectedBottomRightCorner.x, testBox.BottomRight.x, DELTA);
        assertEquals(expectedBottomRightCorner.y, testBox.BottomRight.y, DELTA);
    }
    
    @Test
    public void Alignment_works_on_box() {
        // Arrange
        Coordinate bottomRight = new Coordinate(0.0f, 0.0f);
        Coordinate boxSize = new Coordinate(2.0f, 2.0f);
        
        // Act
        RotatableBox testBox = new RotatableBox();
        testBox.setSize(boxSize);
        testBox.setPosition(bottomRight, RotatableBox.Alignment.BOTTOM_RIGHT);
        
        // Assert
        assertEquals(bottomRight.x, testBox.BottomRight.x, DELTA);
        assertEquals(bottomRight.y, testBox.BottomRight.y, DELTA);
        
        assertEquals(-2.0f, testBox.TopLeft.x, DELTA);
        assertEquals(2.0f, testBox.TopLeft.y, DELTA);
        
        assertEquals(0.0f, testBox.TopRight.x, DELTA);
        assertEquals(2.0f, testBox.TopRight.y, DELTA);
        
        assertEquals(-2.0f, testBox.BottomLeft.x, DELTA);
        assertEquals(0.0f, testBox.BottomLeft.y, DELTA);
    }
    
    @Test
    public void Alignment_works_on_rotated_box() {
        // Arrange
        Coordinate bottomLeft = new Coordinate(-1.0f, -1.0f);
        Coordinate boxSize = new Coordinate(2.0f, 2.0f);
        double fortyFiveDegreesInRadians = Math.PI / 4;
        
        // Act
        RotatableBox testBox = new RotatableBox();
        testBox.setSize(boxSize);
        testBox.setRotation(fortyFiveDegreesInRadians);
        testBox.setPosition(bottomLeft, RotatableBox.Alignment.BOTTOM_LEFT);
        
        // Assert
        assertEquals(bottomLeft.x, testBox.BottomLeft.x, DELTA);
        assertEquals(bottomLeft.y, testBox.BottomLeft.y, DELTA);
        
        Coordinate expectedTopRight = new Coordinate(1.0f, 1.0f);
        expectedTopRight.rotateAroundCoordinate(fortyFiveDegreesInRadians, bottomLeft);
        
        assertEquals(expectedTopRight.x, testBox.TopRight.x, DELTA);
        assertEquals(expectedTopRight.y, testBox.TopRight.y, DELTA);
    }
    
    @Test
    public void Horizontal_mirroring_works() {
        // Arrange
        Coordinate topLeft = new Coordinate(0.2f, -0.4f);
        Coordinate boxSize = new Coordinate(0.1f, 0.1f);
        
        // Act
        RotatableBox nonMirroredTestBox = new RotatableBox(topLeft, boxSize);
        RotatableBox mirroredTestBox = new RotatableBox(topLeft, boxSize);
        mirroredTestBox.setMirroring(true, false);
        
        // Assert
        assertEquals(nonMirroredTestBox.TopRight.x, mirroredTestBox.TopLeft.x, DELTA);
        assertEquals(nonMirroredTestBox.TopRight.y, mirroredTestBox.TopLeft.y, DELTA);
        
        assertEquals(nonMirroredTestBox.TopLeft.x, mirroredTestBox.TopRight.x, DELTA);
        assertEquals(nonMirroredTestBox.TopLeft.y, mirroredTestBox.TopRight.y, DELTA);
        
        assertEquals(nonMirroredTestBox.BottomLeft.x, mirroredTestBox.BottomRight.x, DELTA);
        assertEquals(nonMirroredTestBox.BottomLeft.y, mirroredTestBox.BottomRight.y, DELTA);
        
        assertEquals(nonMirroredTestBox.BottomRight.x, mirroredTestBox.BottomLeft.x, DELTA);
        assertEquals(nonMirroredTestBox.BottomRight.y, mirroredTestBox.BottomLeft.y, DELTA);
    }
    
    @Test
    public void Vertical_mirroring_works() {
        // Arrange
        Coordinate topLeft = new Coordinate(-0.8f, 0.6f);
        Coordinate boxSize = new Coordinate(0.7f, 0.2f);
        
        // Act
        RotatableBox nonMirroredTestBox = new RotatableBox(topLeft, boxSize);
        RotatableBox mirroredTestBox = new RotatableBox(topLeft, boxSize);
        mirroredTestBox.setMirroring(false, true);
        
        // Assert
        assertEquals(nonMirroredTestBox.BottomLeft.x, mirroredTestBox.TopLeft.x, DELTA);
        assertEquals(nonMirroredTestBox.BottomLeft.y, mirroredTestBox.TopLeft.y, DELTA);
        
        assertEquals(nonMirroredTestBox.TopLeft.x, mirroredTestBox.BottomLeft.x, DELTA);
        assertEquals(nonMirroredTestBox.TopLeft.y, mirroredTestBox.BottomLeft.y, DELTA);
        
        assertEquals(nonMirroredTestBox.BottomRight.x, mirroredTestBox.TopRight.x, DELTA);
        assertEquals(nonMirroredTestBox.BottomRight.y, mirroredTestBox.TopRight.y, DELTA);
        
        assertEquals(nonMirroredTestBox.TopRight.x, mirroredTestBox.BottomRight.x, DELTA);
        assertEquals(nonMirroredTestBox.TopRight.y, mirroredTestBox.BottomRight.y, DELTA);
    }
    
    @Test
    public void RotatableBox_Equality_test() {
        Coordinate topLeft = new Coordinate(-0.8f, 0.6f);
        Coordinate boxSize = new Coordinate(0.7f, 0.2f);
        
        RotatableBox identicalBox1 = new RotatableBox(topLeft, boxSize);
        RotatableBox identicalBox2 = new RotatableBox(topLeft, boxSize);
        
        boolean equal = identicalBox1.equals(identicalBox2);
        assertTrue(equal);
    }
    
    @Test
    public void RotatableBox_Inequality_test() {
        Coordinate topLeft1 = new Coordinate(0.0f, 0.1f);
        Coordinate boxSize1 = new Coordinate(0.2f, 0.6f);
        Coordinate topLeft2 = new Coordinate(8.0f, 2.2f);
        Coordinate boxSize2 = new Coordinate(0.1f, 0.1f);
        
        RotatableBox identicalBox1 = new RotatableBox(topLeft1, boxSize1);
        RotatableBox identicalBox2 = new RotatableBox(topLeft2, boxSize2);
        
        boolean equal = identicalBox1.equals(identicalBox2);
        assertFalse(equal);
    }

    @Test
    public void RotatableBox_DeepCopy_test() {
        Coordinate topLeft1 = new Coordinate(0.0f, 0.1f);
        Coordinate boxSize1 = new Coordinate(0.2f, 0.6f);

        RotatableBox identicalBox1 = new RotatableBox(topLeft1, boxSize1);
        RotatableBox identicalBox2 = identicalBox1.deepCopy();

        assertEquals(identicalBox1, identicalBox2);

        identicalBox2.setMirroring(true, false);

        assertNotEquals(identicalBox1, identicalBox2);

        identicalBox2.setMirroring(false, false);
        identicalBox2.TopLeft = new Coordinate(1.0f, 2.0f);

        assertNotEquals(identicalBox1, identicalBox2);

        identicalBox2.setSize(new Coordinate(4.0f, 5.0f));

        assertNotEquals(identicalBox1, identicalBox2);
    }
}
