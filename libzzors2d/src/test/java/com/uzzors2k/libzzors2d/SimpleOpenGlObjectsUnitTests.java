package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.shapes.Point;
import com.uzzors2k.libzzors2d.shapes.ShapesPool;
import com.uzzors2k.libzzors2d.shapes.Line;
import com.uzzors2k.libzzors2d.shapes.Polygon;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleOpenGlObjectsUnitTests {
    private final float DELTA = (float) 0.0001;
    private final PointColor GREEN_COLOR = new PointColor(0, 1.0f, 0, 1.0f);
    private final PointColor BLUE_COLOR = new PointColor(0, 0, 1.0f, 1.0f);
    private final PointColor RED_COLOR = new PointColor(1.0f, 0, 0, 1.0f);
    private final PointColor WHITE_COLOR = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);
    
    @Test
    public void color_object_returns_correct_bytebuffer() {
        float red = (float) 0.5;
        float green = 0;
        float blue = (float) 1.0;
        float alpha = (float) 1.0;
        
        PointColor testColor = new PointColor(red, green, blue, alpha);
        
        float[] bufferData = testColor.getFloatBufferData();
        
        assertEquals(4, bufferData.length);
        assertEquals(red, bufferData[0], DELTA);
        assertEquals(green, bufferData[1], DELTA);
        assertEquals(blue, bufferData[2], DELTA);
        assertEquals(alpha, bufferData[3], DELTA);
    }
    
    @Test
    public void coordinate_object_returns_correct_bytebuffer() {
        float red = (float) 0.2;
        float green = (float) 0.1;
        float blue = (float) 0.8;
        float alpha = (float) 1.0;
        
        PointColor testColor = new PointColor(red, green, blue, alpha);
        Coordinate pos = new Coordinate(-0.3f, 0.7f);
        Point testPoint = new Point(pos, testColor);
        
        float[] bufferData = testPoint.getFloatBufferData();
        
        assertEquals(bufferData.length, 6);
        
        assertEquals(pos.x, bufferData[0], DELTA);
        assertEquals(pos.y, bufferData[1], DELTA);
        
        assertEquals(red, bufferData[2], DELTA);
        assertEquals(green, bufferData[3], DELTA);
        assertEquals(blue, bufferData[4], DELTA);
        assertEquals(alpha, bufferData[5], DELTA);
    }
    
    @Test
    public void line_object_returns_correct_bytebuffer() {
        float red = (float) 0.6;
        float green = (float) 1.0;
        float blue = (float) 0.3;
        float alpha = (float) 0.4;
        
        PointColor testColor = new PointColor(red, green, blue, alpha);
        Coordinate startPos = new Coordinate(-0.3f, 0.7f);
        Coordinate endPos = new Coordinate(0.8f, -0.1f);
        
        Point startPoint = new Point(startPos, testColor);
        Point endPoint = new Point(endPos, testColor);
        
        Line testLine = new Line(startPoint, endPoint, 1);
        
        float[] bufferData = testLine.getFloatBufferData();
        
        assertEquals(bufferData.length, 12);
        
        int index = 0;
        assertEquals(startPos.x, bufferData[index++], DELTA);
        assertEquals(startPos.y, bufferData[index++], DELTA);
        assertEquals(red, bufferData[index++], DELTA);
        assertEquals(green, bufferData[index++], DELTA);
        assertEquals(blue, bufferData[index++], DELTA);
        assertEquals(alpha, bufferData[index++], DELTA);
        
        assertEquals(endPos.x, bufferData[index++], DELTA);
        assertEquals(endPos.y, bufferData[index++], DELTA);
        assertEquals(red, bufferData[index++], DELTA);
        assertEquals(green, bufferData[index++], DELTA);
        assertEquals(blue, bufferData[index++], DELTA);
        assertEquals(alpha, bufferData[index], DELTA);
    }
    
    @Test
    public void drawablePool_can_create_bytebuffer() {
        Point startPoint = new Point(new Coordinate(-0.5f, -0.3f), GREEN_COLOR);
        Point endPoint = new Point(new Coordinate(0.4f, 1.0f), BLUE_COLOR);
        Point derpCoord = new Point(new Coordinate(0.0f, 0.0f), RED_COLOR);
        
        Line testLine1 = new Line(startPoint, endPoint, 1);
        Line testLine2 = new Line(endPoint, startPoint, 1);
        
        ShapesPool testPool = new ShapesPool();
        testPool.addDrawable(testLine1);
        testPool.addDrawable(testLine2);
        testPool.addDrawable(derpCoord);
        testPool.updateTotalFloatArray();

        float[] bufferData = testPool.getFloatBufferData();
        
        assertEquals(30, bufferData.length);
    }
    
    @Test
    public void polygon_can_create_bytebuffer() {
        Point centerPolygonCoord = new Point(new Coordinate(0.5f, 0.5f), GREEN_COLOR);
        Point polygonPoint1 = new Point(new Coordinate(0.7f, 0.5f), BLUE_COLOR);
        Point polygonPoint2 = new Point(new Coordinate(0.7f, 0.7f), WHITE_COLOR);
        
        Polygon testPolygon = new Polygon();
        testPolygon.addPolygonPoint(centerPolygonCoord);
        testPolygon.addPolygonPoint(polygonPoint1);
        testPolygon.addPolygonPoint(polygonPoint2);
        
        float[] bufferData = testPolygon.getFloatBufferData();
        
        assertEquals(18, bufferData.length);
    }
    
    @Test
    public void SimpleObjects_Equality_test() {
        PointColor red1 = new PointColor(1.0f, 0, 0, 1.0f);
        PointColor red2 = new PointColor(1.0f, 0, 0, 1.0f);
        
        boolean equal = red1.equals(red2);
        assertTrue(equal);
        
        Point identicalPoint1 = new Point(new Coordinate(23.8f, 4.5f), red1);
        Point identicalPoint2 = new Point(new Coordinate(23.8f, 4.5f), red2);
        
        equal = identicalPoint1.equals(identicalPoint2);
        assertTrue(equal);
        
        Point startPoint = new Point(new Coordinate(1.0f, 2.0f), red1);
        Point endPoint = new Point(new Coordinate(2.0f, 1.0f), red2);
        
        Line identicalLine1 = new Line(startPoint, endPoint, 1);
        Line identicalLine2 = new Line(startPoint, endPoint, 1);
        
        equal = identicalLine1.equals(identicalLine2);
        assertTrue(equal);
    }
    
    @Test
    public void SimpleObjects_Inequality_test() {
        PointColor red1 = new PointColor(1.0f, 0, 0, 1.0f);
        PointColor green = new PointColor(0.9f, 1.0f, 0, 1.0f);
        
        boolean equal = red1.equals(green);
        assertFalse(equal);
        
        Point identicalPoint1 = new Point(new Coordinate(23.8f, 4.5f), red1);
        Point identicalPoint2 = new Point(new Coordinate(23.8f, 4.5f), green);
        
        equal = identicalPoint1.equals(identicalPoint2);
        assertFalse(equal);
        
        Point startPoint = new Point(new Coordinate(1.0f, 2.0f), red1);
        Point endPoint = new Point(new Coordinate(2.0f, 1.0f), red1);
        
        Line identicalLine1 = new Line(startPoint, endPoint, 1);
        Line identicalLine2 = new Line(endPoint, startPoint, 1);
        
        equal = identicalLine1.equals(identicalLine2);
        assertFalse(equal);
    }
    
    @Test
    public void Polygon_Equality_test() {
        Point centerPolygonCoord = new Point(new Coordinate(0.5f, 0.5f), GREEN_COLOR);
        Point polygonPoint1 = new Point(new Coordinate(0.7f, 0.5f), BLUE_COLOR);
        Point polygonPoint2 = new Point(new Coordinate(0.7f, 0.7f), WHITE_COLOR);
        
        Polygon identicalPolygon1 = new Polygon();
        identicalPolygon1.addPolygonPoint(centerPolygonCoord);
        identicalPolygon1.addPolygonPoint(polygonPoint1);
        identicalPolygon1.addPolygonPoint(polygonPoint2);
        
        Polygon identicalPolygon2 = new Polygon();
        identicalPolygon2.addPolygonPoint(centerPolygonCoord);
        identicalPolygon2.addPolygonPoint(polygonPoint1);
        identicalPolygon2.addPolygonPoint(polygonPoint2);
        
        boolean equal = identicalPolygon1.equals(identicalPolygon2);
        assertTrue(equal);
    }
    
    @Test
    public void Polygon_Inequality_test() {
        Point centerPolygonCoord = new Point(new Coordinate(0.5f, 0.5f), GREEN_COLOR);
        Point polygonPoint1 = new Point(new Coordinate(0.7f, 0.5f), BLUE_COLOR);
        Point polygonPoint2 = new Point(new Coordinate(0.7f, 0.7f), WHITE_COLOR);
        
        Polygon identicalPolygon1 = new Polygon();
        identicalPolygon1.addPolygonPoint(centerPolygonCoord);
        identicalPolygon1.addPolygonPoint(polygonPoint1);
        identicalPolygon1.addPolygonPoint(polygonPoint2);
        
        Polygon identicalPolygon2 = new Polygon();
        identicalPolygon2.addPolygonPoint(centerPolygonCoord);
        identicalPolygon2.addPolygonPoint(polygonPoint1);
        // Swap point 2 with pont 1 again
        identicalPolygon2.addPolygonPoint(polygonPoint1);
        
        boolean equal = identicalPolygon1.equals(identicalPolygon2);
        assertFalse(equal);
    }
    
    @Test
    public void Polygon_can_remove_points() {
        // Arrange
        Point centerPolygonCoord = new Point(new Coordinate(0.1f, 0.2f), GREEN_COLOR);
        Point polygonPoint1 = new Point(new Coordinate(0.3f, 0.4f), BLUE_COLOR);
        Point polygonPoint2 = new Point(new Coordinate(0.5f, 0.6f), WHITE_COLOR);
        
        Polygon polygonWithRemovedPoint = new Polygon();
        polygonWithRemovedPoint.addPolygonPoint(centerPolygonCoord);
        polygonWithRemovedPoint.addPolygonPoint(polygonPoint1);
        polygonWithRemovedPoint.addPolygonPoint(polygonPoint2);
        
        // Act
        polygonWithRemovedPoint.removePoint(centerPolygonCoord);
        
        // Assert
        float[] bufferData = polygonWithRemovedPoint.getFloatBufferData();
        
        assertEquals(12, bufferData.length);
        
        int index = 0;
        assertEquals(polygonPoint1.location.x, bufferData[index++], DELTA);
        assertEquals(polygonPoint1.location.y, bufferData[index++], DELTA);
        assertEquals(BLUE_COLOR.getRedComponent(), bufferData[index++], DELTA);
        assertEquals(BLUE_COLOR.getGreenComponent(), bufferData[index++], DELTA);
        assertEquals(BLUE_COLOR.getBlueComponent(), bufferData[index++], DELTA);
        assertEquals(BLUE_COLOR.getAlphaComponent(), bufferData[index++], DELTA);
        
        assertEquals(polygonPoint2.location.x, bufferData[index++], DELTA);
        assertEquals(polygonPoint2.location.y, bufferData[index++], DELTA);
        assertEquals(WHITE_COLOR.getRedComponent(), bufferData[index++], DELTA);
        assertEquals(WHITE_COLOR.getGreenComponent(), bufferData[index++], DELTA);
        assertEquals(WHITE_COLOR.getBlueComponent(), bufferData[index++], DELTA);
        assertEquals(WHITE_COLOR.getAlphaComponent(), bufferData[index], DELTA);
    }
}
