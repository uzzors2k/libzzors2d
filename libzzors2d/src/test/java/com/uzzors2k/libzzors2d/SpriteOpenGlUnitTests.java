package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.shapes.PointColor;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.sprites.Sprite;
import com.uzzors2k.libzzors2d.sprites.SpritePool;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for all sprite related stuff
 */

public class SpriteOpenGlUnitTests {
    private final float DELTA = (float) 0.0001;
    private final int SPRITE_SIZE = 32;
    private final int SPRITE_SCREEN_COORD_OFFSET = 0;
    private final int SPRITE_ALTAS_COORD_OFFSET = 2;
    private final int SPRITE_CORNER_OFFSET = 8;
    // TODO: Unit test the half pixel logic!
    private final Coordinate HALF_PIXEL = new Coordinate(0, 0);

    @Test
    public void sprite_can_be_created_correctly() {
        // Create a 8x8 simple cell atlas
        int frameColumns = 8;
        int frameRows = 8;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));
        PointColor topLeft = new PointColor(1.0f, 1.0f, 1.0f, 0.1f);
        PointColor topRight = new PointColor(1.0f, 1.0f, 1.0f, 0.2f);
        PointColor bottomLeft = new PointColor(1.0f, 1.0f, 1.0f, 0.3f);
        PointColor bottomRight = new PointColor(1.0f, 1.0f, 1.0f, 0.4f);
        
        Sprite fullAtlasSprite = new Sprite(0, 0, frameColumns, frameRows, 1, 1, HALF_PIXEL);
        fullAtlasSprite.setAtlasCellSize(atlasCellSize);
        fullAtlasSprite.setCurrentFrame(0);
        fullAtlasSprite.setColorFilter(topLeft, topRight, bottomLeft, bottomRight);
        
        float[] bufferData = fullAtlasSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check that the atlas buffer uses the entire atlas
        assertEquals(0f, bufferData[SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(1f, bufferData[SPRITE_ALTAS_COORD_OFFSET+1], DELTA);
        
        assertEquals(1f, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(0f, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET + 1], DELTA);

        final int ALPHA_COMPONENT_OFFSET = 3;

        int SPRITE_COLOR_OFFSET = 4;
        assertEquals(bottomLeft.getAlphaComponent(), bufferData[SPRITE_COLOR_OFFSET + ALPHA_COMPONENT_OFFSET], DELTA);
        assertEquals(topLeft.getAlphaComponent(), bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_COLOR_OFFSET + ALPHA_COMPONENT_OFFSET], DELTA);
        assertEquals(bottomRight.getAlphaComponent(), bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_COLOR_OFFSET + ALPHA_COMPONENT_OFFSET], DELTA);
        assertEquals(topRight.getAlphaComponent(), bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_COLOR_OFFSET + ALPHA_COMPONENT_OFFSET], DELTA);
        assertEquals(fullAtlasSprite.getColoringMode(), 1);
    }
    
    @Test
    public void sprite_can_change_frames() {
        // Create a 4x4 simple cell atlas
        int frameColumns = 4;
        int frameRows = 4;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));
        
        Sprite testSprite = new Sprite(0, 0, 1, 1, frameColumns, frameRows, HALF_PIXEL);
        testSprite.setAtlasCellSize(atlasCellSize);
        float[] bufferData = testSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check that sections of the bitmap atlas are selected correctly
        int currentFrame = 2;
        testSprite.setCurrentFrame(currentFrame);
        bufferData = testSprite.getFloatBufferData();
        
        Coordinate expectedTopLeft = new Coordinate(0.5f, 0.25f);
        assertEquals(expectedTopLeft.x, bufferData[SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[SPRITE_ALTAS_COORD_OFFSET+1], DELTA);
        
        Coordinate expectedBottomRight = new Coordinate(expectedTopLeft.x + atlasCellSize.x,
                expectedTopLeft.y - atlasCellSize.y);
        assertEquals(expectedBottomRight.x, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedBottomRight.y, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET + 1], DELTA);
        
        // Try changing frame
        currentFrame = 8;
        testSprite.setCurrentFrame(currentFrame);
        bufferData = testSprite.getFloatBufferData();
        
        expectedTopLeft = new Coordinate(0, 0.75f);
        assertEquals(expectedTopLeft.x, bufferData[SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[SPRITE_ALTAS_COORD_OFFSET+1], DELTA);
        
        expectedBottomRight = new Coordinate(expectedTopLeft.x + atlasCellSize.x,
                expectedTopLeft.y - atlasCellSize.y);
        assertEquals(expectedBottomRight.x, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedBottomRight.y, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET + 1], DELTA);
        
        // Try changing frame
        currentFrame = 15;
        testSprite.setCurrentFrame(currentFrame);
        bufferData = testSprite.getFloatBufferData();
        
        expectedTopLeft = new Coordinate(0.75f, 1.0f);
        assertEquals(expectedTopLeft.x, bufferData[SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[SPRITE_ALTAS_COORD_OFFSET+1], DELTA);
        
        expectedBottomRight = new Coordinate(expectedTopLeft.x + atlasCellSize.x,
                expectedTopLeft.y - atlasCellSize.y);
        assertEquals(expectedBottomRight.x, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedBottomRight.y, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET + 1], DELTA);
    }
    
    @Test
    public void sprite_can_change_position() {
        Sprite positionTestSprite = new Sprite(0, 0, 1, 1, 6, 6, HALF_PIXEL);
        Coordinate position = new Coordinate(0f, 0f);
        Coordinate size = new Coordinate(1.0f, 1.0f);
        positionTestSprite.setSpriteSize(size);
        positionTestSprite.setPosition(position, RotatableBox.Alignment.TOP_LEFT);
        
        float[] bufferData = positionTestSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check top left corner
        assertEquals(position.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        // Check bottom right corner
        assertEquals(position.x + size.x, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y - size.y, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        // Check a new position and size
        position.x = -0.7f;
        position.y = 0.9f;
        size.x = 0.2f;
        size.y = 0.3f;
        //positionTestSprite.setSpriteSize(size); // Don't need an explicit call, pass by reference
        positionTestSprite.setPosition(position, RotatableBox.Alignment.TOP_RIGHT);
        
        bufferData = positionTestSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check top left corner
        assertEquals(position.x - size.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        // Check bottom right corner
        assertEquals(position.x, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y - size.y, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
    }
    
    @Test
    public void sprite_can_select_correct_text_atlas_frame() {
        int frameColumns = 15;
        int frameRows = 11;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));
        
        Sprite testSprite = new Sprite(0, 0, 1, 1, frameColumns, frameRows, HALF_PIXEL);
        testSprite.setAtlasCellSize(atlasCellSize);
        testSprite.setCurrentFrame(33);        // Should be character 'A' in the bitmap
        
        float[] bufferData = testSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        Coordinate expectedTopLeft = new Coordinate(atlasCellSize.x * 3, (atlasCellSize.y *
                3));
        assertEquals(expectedTopLeft.x, bufferData[SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[SPRITE_ALTAS_COORD_OFFSET+1], DELTA);
        
        Coordinate expectedBottomRight = new Coordinate(expectedTopLeft.x + atlasCellSize.x,
                expectedTopLeft.y - atlasCellSize.y);
        assertEquals(expectedBottomRight.x, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET], DELTA);
        assertEquals(expectedBottomRight.y, bufferData[(3 * SPRITE_CORNER_OFFSET) + SPRITE_ALTAS_COORD_OFFSET + 1], DELTA);
    }
    
    @Test
    public void spritePool_can_redraw_external_objects() {
        SpritePool testSpritePool = new SpritePool(2, 1);
        Sprite testSprite = new Sprite(0, 0, 1, 1, 2, 1, HALF_PIXEL);
        testSprite.setCurrentFrame(0);
        testSpritePool.addDrawable(testSprite);
        testSpritePool.updateTotalFloatArray();

        float[] bufferData = testSpritePool.getFloatBufferData();
        
        // Verify that size and initial frame are all correct
        assertEquals(SPRITE_SIZE, bufferData.length);
        Coordinate expectedTopLeft = new Coordinate(0.0f, 1.0f);
        assertEquals(expectedTopLeft.x, bufferData[2], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[3], DELTA);
        
        // Try changing the frame of the original object. Want the sprite pool to update also
        testSprite.setCurrentFrame(1);
        testSpritePool.updateTotalFloatArray();
        bufferData = testSpritePool.getFloatBufferData();
        
        expectedTopLeft = new Coordinate(0.5f, 1.0f);
        assertEquals(expectedTopLeft.x, bufferData[2], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[3], DELTA);
    }
    
    @Test
    public void sprite_alignment_works() {
        Sprite positionTestSprite = new Sprite(0, 0, 1, 1, 6, 6, HALF_PIXEL);
        Coordinate position = new Coordinate(0f, 0f);
        Coordinate size = new Coordinate(1.0f, 1.0f);
        
        positionTestSprite.setSpriteSize(size);
        positionTestSprite.setPosition(position, RotatableBox.Alignment.BOTTOM_RIGHT);
        
        float[] bufferData = positionTestSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check top left corner
        assertEquals(position.x - size.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y + size.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        // Check bottom right corner
        assertEquals(position.x, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        // Check a new position and size
        position.x = -0.7f;
        position.y = 0.9f;
        size.x = 0.2f;
        size.y = 0.3f;
        positionTestSprite.setSpriteSize(size);
        positionTestSprite.setPosition(position, RotatableBox.Alignment.BOTTOM_LEFT);
        
        bufferData = positionTestSprite.getFloatBufferData();
        assertEquals(SPRITE_SIZE, bufferData.length);
        
        // Check top left corner
        assertEquals(position.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y + size.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        // Check bottom right corner
        assertEquals(position.x + size.x, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(position.y, bufferData[(2 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
    }
    
    @Test
    public void Sprite_Equality_test() {
        // Create a 8x8 simple cell atlas
        int frameColumns = 8;
        int frameRows = 8;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));
        
        Sprite identicalSprite1 = new Sprite(0, 0, frameColumns, frameRows, 2, 1, HALF_PIXEL);
        identicalSprite1.setAtlasCellSize(atlasCellSize);
        identicalSprite1.setCurrentFrame(0);
        identicalSprite1.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .BOTTOM_CENTER);
        
        Sprite identicalSprite2 = new Sprite(0, 0, frameColumns, frameRows, 2, 1, HALF_PIXEL);
        identicalSprite2.setAtlasCellSize(atlasCellSize);
        identicalSprite2.setCurrentFrame(0);
        identicalSprite2.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .BOTTOM_CENTER);
        
        boolean equal = identicalSprite1.equals(identicalSprite2);
        assertTrue(equal);
    }
    
    @Test
    public void Sprite_Inequality_test() {
        // Create a 8x8 simple cell atlas
        int frameColumns = 8;
        int frameRows = 8;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));
        
        Sprite identicalSprite1 = new Sprite(0, 0, frameColumns, frameRows, 2, 1, HALF_PIXEL);
        identicalSprite1.setAtlasCellSize(atlasCellSize);
        identicalSprite1.setCurrentFrame(0);
        identicalSprite1.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .BOTTOM_CENTER);
        
        Sprite identicalSprite2 = new Sprite(0, 0, frameColumns, frameRows, 2, 1, HALF_PIXEL);
        identicalSprite2.setAtlasCellSize(atlasCellSize);
        // Change the current frame
        identicalSprite2.setCurrentFrame(1);
        identicalSprite2.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .BOTTOM_CENTER);
        
        boolean equal = identicalSprite1.equals(identicalSprite2);
        assertFalse(equal);
    }
    
    @Test
    public void Sprite_can_be_removed_from_spritepool() {
        // Arrange
        SpritePool testSpritePool = new SpritePool(2, 1);
        Sprite testSprite = new Sprite(0, 0, 1, 1, 2, 1, HALF_PIXEL);
        testSprite.setCurrentFrame(0);
        testSpritePool.addDrawable(testSprite);
        
        Sprite removedSprite = new Sprite(0, 0, 1, 1, 4, 1, HALF_PIXEL);
        removedSprite.setCurrentFrame(1);
        testSpritePool.addDrawable(removedSprite);
        
        // Act
        testSpritePool.removeObject(removedSprite);
        testSpritePool.updateTotalFloatArray();
        
        // Assert
        float[] bufferData = testSpritePool.getFloatBufferData();
        
        // Verify that size and initial frame are all correct
        assertEquals(SPRITE_SIZE, bufferData.length);
        Coordinate expectedTopLeft = new Coordinate(0.0f, 1.0f);
        assertEquals(expectedTopLeft.x, bufferData[2], DELTA);
        assertEquals(expectedTopLeft.y, bufferData[3], DELTA);
    }

    @Test
    public void Sprite_DeepCopy_test() {
        // Create a 8x8 simple cell atlas
        int frameColumns = 8;
        int frameRows = 8;
        Coordinate atlasCellSize = new Coordinate((1.0f / frameColumns), (1.0f / frameRows));

        Sprite identicalSprite1 = new Sprite(0, 0, frameColumns, frameRows, 2, 1, HALF_PIXEL);
        identicalSprite1.setAtlasCellSize(atlasCellSize);
        identicalSprite1.setCurrentFrame(0);
        identicalSprite1.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .BOTTOM_CENTER);

        Sprite identicalSprite2 = identicalSprite1.deepCopy();

        assertEquals(identicalSprite1, identicalSprite2);

        // Change the current frame
        identicalSprite2.setCurrentFrame(1);
        identicalSprite2.setPosition(new Coordinate(4.2f, 3.2f), RotatableBox.Alignment
                .TOP_RIGHT);

        assertNotEquals(identicalSprite1, identicalSprite2);
    }

    @Test
    public void Sprite_Color_filter_works() {
        PointColor replacementColor = new PointColor(1.0f, 1.0f, 1.0f, 1.0f);

        Sprite uniformColorReplacementSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);
        Sprite cornerColorReplacementSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);
        Sprite blueReplacementSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);
        Sprite redReplacementSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);
        Sprite greenReplacementSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);
        Sprite noColorFilteringSprite = new Sprite(0, 0, 1, 1, 1, 1, HALF_PIXEL);

        uniformColorReplacementSprite.setColorFilter(replacementColor);
        cornerColorReplacementSprite.setColorFilter(replacementColor, replacementColor, replacementColor, replacementColor);
        blueReplacementSprite.setBlueReplacement(replacementColor);
        redReplacementSprite.setRedReplacement(replacementColor);
        greenReplacementSprite.setGreenReplacement(replacementColor);

        assertEquals(noColorFilteringSprite.getColoringMode(), 0);
        assertEquals(uniformColorReplacementSprite.getColoringMode(), 1);
        assertEquals(cornerColorReplacementSprite.getColoringMode(), 1);
        assertEquals(blueReplacementSprite.getColoringMode(), 8);
        assertEquals(redReplacementSprite.getColoringMode(), 2);
        assertEquals(greenReplacementSprite.getColoringMode(), 4);
    }
}
