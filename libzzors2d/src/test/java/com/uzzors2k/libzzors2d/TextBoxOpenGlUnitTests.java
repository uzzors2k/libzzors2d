package com.uzzors2k.libzzors2d;

import com.uzzors2k.libzzors2d.utils.complexDataTypes.RotatableBox;
import com.uzzors2k.libzzors2d.text.TextAtlasProperties;
import com.uzzors2k.libzzors2d.text.TextBox;
import com.uzzors2k.libzzors2d.text.TextTextureCreator;
import com.uzzors2k.libzzors2d.utils.complexDataTypes.Coordinate;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for the text box objects
 */

public class TextBoxOpenGlUnitTests {
    private final float DELTA = (float) 0.0001;
    private final int SPRITE_SIZE = 32;
    private final int SPRITE_SCREEN_COORD_OFFSET = 0;
    private final int SPRITE_ALTAS_COORD_OFFSET = 2;
    private final int SPRITE_ALPHA_OFFSET = 4;
    private final int SPRITE_COLOR_OFFSET = 5;
    private final int SPRITE_CORNER_OFFSET = 8;
    private final Coordinate HALF_PIXEL = new Coordinate(0, 0);
    
    @Test
    public void textBox_works() throws Exception {
        // Arrange
        String testString = "Test 123 {|}~";
        float charSizeX = 0.1f;
        float charSizeY = 0.1f;
        float[] charWidths = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths, 1.0f); // All characters the same size
        
        // Act
        TextAtlasProperties textAtlasProperties =
                new TextAtlasProperties(columns, rows, charWidths,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        TextBox testText = new TextBox(textAtlasProperties);
        testText.setText(testString);
        testText.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        testText.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.TOP_LEFT);
        
        // Assert
        float[] bufferData = testText.getFloatBufferData();
        assertEquals(SPRITE_SIZE * testString.length(), bufferData.length);
        
        Coordinate expectedTopLeftFirstChar = new Coordinate(0.0f, 0.0f);
        assertEquals(expectedTopLeftFirstChar.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeftFirstChar.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        Coordinate expectedTopLeftLastChar = new Coordinate(charSizeX * (testString.length() - 1),
                0.0f);
        int topLeftXindex = SPRITE_SIZE * testString.length() - (SPRITE_SIZE - (1 * SPRITE_CORNER_OFFSET));
        int topLeftYindex = topLeftXindex + 1;
        assertEquals(expectedTopLeftLastChar.x, bufferData[topLeftXindex], DELTA);
        assertEquals(expectedTopLeftLastChar.y, bufferData[topLeftYindex], DELTA);
    }
    
    @Test
    public void textBox_alignment_works() throws Exception {
        // Arrange
        String testString = "HERP";
        float charSizeX = 0.1f;
        float charSizeY = 0.1f;
        float[] charWidths = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths, 1.0f); // All characters the same size
        
        // Act
        TextAtlasProperties textAtlasProperties =
                new TextAtlasProperties(columns, rows, charWidths,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        TextBox testText = new TextBox(textAtlasProperties);
        testText.setText(testString);
        testText.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        testText.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.BOTTOM_RIGHT);
        
        // Assert
        float[] bufferData = testText.getFloatBufferData();
        assertEquals(SPRITE_SIZE * testString.length(), bufferData.length);
        
        Coordinate expectedTopLeftFirstChar = new Coordinate(-testString.length() * charSizeX,
                charSizeY);
        assertEquals(expectedTopLeftFirstChar.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeftFirstChar.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        Coordinate expectedBottomRightLastChar = new Coordinate(0.0f, 0.0f);
        int bottomRightXindex = SPRITE_SIZE * testString.length() - (SPRITE_SIZE - (2 * SPRITE_CORNER_OFFSET));
        int bottomRightYindex = bottomRightXindex + 1;
        assertEquals(expectedBottomRightLastChar.x, bufferData[bottomRightXindex], DELTA);
        assertEquals(expectedBottomRightLastChar.y, bufferData[bottomRightYindex], DELTA);
    }
    
    @Test
    public void textBox_rotation_works() throws Exception {
        // Arrange
        double rotation = Math.PI / 4;
        String testString = "TEe";
        float charSizeX = 1.0f;
        float charSizeY = 1.0f;
        float[] charWidths = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths, 1.0f); // All characters the same size
        
        // Act
        TextAtlasProperties textAtlasProperties =
                new TextAtlasProperties(columns, rows, charWidths,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        TextBox testText = new TextBox(textAtlasProperties);
        testText.setText(testString);
        testText.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        testText.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.BOTTOM_LEFT);
        testText.setTextBoxRotationRadians(rotation);
        
        // Assert
        float[] bufferData = testText.getFloatBufferData();
        assertEquals(SPRITE_SIZE * testString.length(), bufferData.length);
        
        Coordinate expectedTopLeftFirstChar = new Coordinate(0.0f, charSizeY);
        expectedTopLeftFirstChar.rotateAroundOrigin(rotation);
        
        assertEquals(expectedTopLeftFirstChar.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeftFirstChar.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        Coordinate expectedBottomRightLastChar = new Coordinate(testString.length() * charSizeX,
                0.0f);
        expectedBottomRightLastChar.rotateAroundOrigin(rotation);
        
        int bottomRightXindex = SPRITE_SIZE * testString.length() - (SPRITE_SIZE - (2 * SPRITE_CORNER_OFFSET));
        int bottomRightYindex = bottomRightXindex + 1;
        assertEquals(expectedBottomRightLastChar.x, bufferData[bottomRightXindex], DELTA);
        assertEquals(expectedBottomRightLastChar.y, bufferData[bottomRightYindex], DELTA);
    }
    
    @Test
    public void textBox_kerning_works() throws Exception {
        // Arrange
        String testString = "TieFighter";
        float charSizeX = 1.0f;
        float charSizeY = 1.0f;
        float[] charWidths = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths, 0.5f); // All characters the same size, but half of the char size
        
        // Act
        TextAtlasProperties textAtlasProperties =
                new TextAtlasProperties(columns, rows, charWidths,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        TextBox testText = new TextBox(textAtlasProperties);
        testText.setText(testString);
        testText.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        testText.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment.BOTTOM_LEFT);
        
        // Assert
        float[] bufferData = testText.getFloatBufferData();
        assertEquals(SPRITE_SIZE * testString.length(), bufferData.length);
        
        Coordinate expectedTopLeftFirstChar = new Coordinate(0.0f, charSizeY);
        
        assertEquals(expectedTopLeftFirstChar.x, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET], DELTA);
        assertEquals(expectedTopLeftFirstChar.y, bufferData[(1 * SPRITE_CORNER_OFFSET) + SPRITE_SCREEN_COORD_OFFSET + 1], DELTA);
        
        // Kerning isn't currently applied to the first or last character,
        // since the characters are written sequentially, the first character is
        // overlapped and thus "kerned", whereas the last is the full size
        // The equation below is only valid when all characters have the same kerning size
        float expectedWidth = charSizeX * (((testString.length() - 1) * charWidths[0]) + 1);
        Coordinate expectedBottomRightLastChar = new Coordinate(expectedWidth, 0.0f);
        
        int bottomRightXindex = SPRITE_SIZE * testString.length() - (SPRITE_SIZE - (2 * SPRITE_CORNER_OFFSET));
        int bottomRightYindex = bottomRightXindex + 1;
        assertEquals(expectedBottomRightLastChar.x, bufferData[bottomRightXindex], DELTA);
        assertEquals(expectedBottomRightLastChar.y, bufferData[bottomRightYindex], DELTA);
    }
    
    @Test
    public void TextBox_Equality_test() throws Exception {
        // Arrange
        String testString = "TieFighter";
        float charSizeX = 1.0f;
        float charSizeY = 1.0f;
        float[] charWidths = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths, 0.5f); // All characters the same size, but half of the char size
        TextAtlasProperties textAtlasProperties =
                new TextAtlasProperties(columns, rows, charWidths,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        
        TextBox indenticalTextbox1 = new TextBox(textAtlasProperties);
        indenticalTextbox1.setText(testString);
        indenticalTextbox1.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        indenticalTextbox1.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment
                .BOTTOM_LEFT);
        
        TextBox indenticalTextbox2 = new TextBox(textAtlasProperties);
        indenticalTextbox2.setText(testString);
        indenticalTextbox2.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        indenticalTextbox2.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment
                .BOTTOM_LEFT);
        
        // Assert
        boolean equal = indenticalTextbox1.equals(indenticalTextbox2);
        assertTrue(equal);
    }
    
    @Test
    public void TextBox_Inequality_test() throws Exception {
        // Arrange
        String testString = "TieFighter";
        float charSizeX = 1.0f;
        float charSizeY = 1.0f;
        float[] charWidths1 = new float[TextTextureCreator.CHAR_END + 1];
        float[] charWidths2 = new float[TextTextureCreator.CHAR_END + 1];
        int columns = 15;
        int rows = 14;
        Coordinate cellSize = new Coordinate(1.0f / columns, 1.0f / rows);
        
        Arrays.fill(charWidths1, 0.5f); // All characters the same size, but half of the char size
        TextAtlasProperties textAtlasProperties1 =
                new TextAtlasProperties(columns, rows, charWidths1,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        
        TextBox indenticalTextbox1 = new TextBox(textAtlasProperties1);
        indenticalTextbox1.setText(testString);
        indenticalTextbox1.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        indenticalTextbox1.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment
                .BOTTOM_LEFT);
        
        // Slightly different kerning sizes
        Arrays.fill(charWidths2, 0.4f); // All characters the same size, but half of the char size
        TextAtlasProperties textAtlasProperties2 =
                new TextAtlasProperties(columns, rows, charWidths2,
                        TextTextureCreator.CHAR_START, TextTextureCreator.CHAR_END, cellSize, HALF_PIXEL);
        
        TextBox indenticalTextbox2 = new TextBox(textAtlasProperties2);
        indenticalTextbox2.setText(testString);
        indenticalTextbox2.setCharacterSize(new Coordinate(charSizeX, charSizeY));
        indenticalTextbox2.setPosition(new Coordinate(0.0f, 0.0f), RotatableBox.Alignment
                .BOTTOM_LEFT);
        
        // Assert
        boolean equal = indenticalTextbox1.equals(indenticalTextbox2);
        assertFalse(equal);
    }
}
